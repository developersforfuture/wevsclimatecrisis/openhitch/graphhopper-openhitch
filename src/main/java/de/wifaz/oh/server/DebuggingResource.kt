package de.wifaz.oh.server

import com.fasterxml.jackson.databind.InjectableValues
import com.fasterxml.jackson.databind.ObjectMapper
import de.wifaz.oh.protocol.Feedback
import de.wifaz.oh.protocol.User
import org.apache.commons.lang3.StringUtils
import org.slf4j.LoggerFactory
import java.io.File
import java.net.HttpURLConnection
import java.nio.file.Paths
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.UriBuilder
import kotlin.collections.ArrayList

@Path("debug")
class DebuggingResource(private val db: HitchDB) {
    private val SERVER_ID_FILE = "debug-server-id.txt"
    private val serverId: String by lazy { initServerId() }

    @POST
    @Path("dump")
    fun dump(@FormParam("name") name: String, @Context request: HttpServletRequest): Response {
        restrictToLocalhost(request)
        val dbDump = db.dump()
        val mapper = ObjectMapper()
        val path = Paths.get("dumps", "${name}.json")
        val file = path.toFile()
        mapper.writeValue(file, dbDump)
        logger.info("Dumped database to $path")
        return redirectToDbm()
    }

    private fun configObjectMapper(mapper:ObjectMapper) {
        // This is to support reading old databases
        // Every new attribute of class DBDump or its substructures has to be marked with @JacksonInject
        // and the attribute needs to be added here
        val injectableValues = InjectableValues.Std()
        injectableValues.addValue("feedbacks", ArrayList<Feedback>())
        mapper.setInjectableValues(injectableValues)
    }

    private fun redirectToDbm(): Response {
        val uri = UriBuilder.fromUri("/debug/dbm").build()
        return Response.seeOther(uri).build()
    }

    @GET
    @Path("showdb")
    @Produces(MediaType.APPLICATION_JSON)
    fun showdb(@Context request: HttpServletRequest): String {
        restrictToLocalhost(request)
        val dbDump = db.dump()
        val mapper = ObjectMapper()
        return mapper.writeValueAsString(dbDump)
    }

    @GET
    @Path("showfile/{filename}")
    @Produces(MediaType.APPLICATION_JSON)
    fun showfile(@PathParam("filename") filename: String, @Context request: HttpServletRequest): String {
        restrictToLocalhost(request)
        val mapper = ObjectMapper()
        configObjectMapper(mapper)
        val path = Paths.get("dumps", filename)
        val file = path.toFile()
        val dbDump: DBDump = mapper.readValue(file, DBDump::class.java)
        return mapper.writeValueAsString(dbDump)
    }


    @GET
    @Path("dbm")
    @Produces(MediaType.TEXT_HTML)
    fun dbManager(@Context request: HttpServletRequest): String {
        restrictToLocalhost(request)
        return form()
    }

    private fun form(): String {
        val folder = File("dumps")
        var html = """<html><head><title>OpenHitch test database manager</title></head>
            <body>
            <h3>OpenHitch test database manager</h3> 
            <form method="post" action="dump">
                Write current content of database to
                <input type="text" name="name">.json
                <input type="submit" value="Write">
            </form>
            <br/>
            <p>The following database dumps are available at <code>${folder.absolutePath}</code> .</p>
            <table>
            """
        folder.listFiles().forEach {
            html += """
                <tr valign="top">
                    <td>${it.name}</td>
                    <td><form method="get" action="showfile/${it.name}">
                        <input type="submit" value="Show">
                    </form></td>
                    <td><form method="post" action="rwts">
                        <input type="hidden" name="filename" value="${it.name}">
                        <input type="submit" value="Read">
                    </form></td>
                    <td><form method="post" action="delete">
                        <input type="hidden" name="filename" value="${it.name}">
                        <input type="submit" value="Delete">
                    </form></td>
                </tr>"""
        }
        html += """
            </table>
            <p>Button "Read" erases the database attached to the running OpenHitch server and replaces it with the content of the respective file.<br/>
             Button "Delete" deletes the file.</p>
            <br/>
            <form method="get" action="showdb">
                <input type="submit" value="Show current content of database">
            </form>            
            </body></html>"""
        return html.trimIndent()
    }

    @POST
    @Path("rwts")
    fun restoreWithTimeShift(@FormParam("filename") filename: String, @Context request: HttpServletRequest): Response {
        restrictToLocalhost(request)
        val mapper = ObjectMapper()
        configObjectMapper(mapper)
        val path = Paths.get("dumps", filename)
        val file = path.toFile()
        var dbDump: DBDump = mapper.readValue(file, DBDump::class.java)
        var originalTime = dbDump.time
        dbDump = timeShift(dbDump)
        dbDump = uniquifyPersonaIds(dbDump)
        db.restore(dbDump)
        logger.info("Read database from ${path}, with time shift ${Date(originalTime)} -> ${Date(dbDump.time)}")
        return redirectToDbm()
    }

    @POST
    @Path("delete")
    fun deleteFile(@FormParam("filename") filename: String, @Context request: HttpServletRequest): Response {
        restrictToLocalhost(request)
        val path = Paths.get("dumps", filename)
        val file = path.toFile()
        file.delete()
        return redirectToDbm()
    }

    private fun restrictToLocalhost(request: HttpServletRequest) {
        if (request.remoteAddr != "127.0.0.1") {
            throw WebApplicationException(
                "This service can be accessed only from localhost",
                HttpURLConnection.HTTP_FORBIDDEN
            )
        }
    }

    private fun timeShift(dbDump: DBDump): DBDump {
        val now = Date().time
        val delta = now - dbDump.time

        val shiftedWays = dbDump.ways.map {
            it.copy(
                start_time = it.start_time + delta,
                end_time = it.end_time + delta
            )
        }
        val shiftedLifts = dbDump.lifts.map {
            it.copy(
                pickup_time = it.pickup_time + delta,
                drop_off_time = it.drop_off_time + delta
            )
        }
        return dbDump.copy(
            time = now,
            ways = java.util.ArrayList(shiftedWays),
            lifts = java.util.ArrayList(shiftedLifts)
        )
    }

    private fun uniquifyPersonaIds(dbDump: DBDump): DBDump {
        val newUsers = dbDump.users.map { it.copy(id = uniquify(it.id)) }
        val userTokens = dbDump.user_tokens.map {
            val uid = uniquify(it.userId);
            UserToken(uid, TokenHelper.asDbHash(uid.toByteArray()))
        }
        val registrationStates = dbDump.registration_states.map { it.copy(user_id = uniquify(it.user_id)) }
        val ways = dbDump.ways.map { it.copy(user_id = uniquify(it.user_id)) }
        val profilePictures = dbDump.profile_pictures.mapKeys { uniquify(it.key) }
        return DBDump(
            time = dbDump.time,
            users = ArrayList(newUsers),
            user_tokens = ArrayList(userTokens),
            registration_states = ArrayList(registrationStates),
            ways = ArrayList(ways),
            lifts = dbDump.lifts,
            profile_pictures = profilePictures,
            feedbacks = dbDump.feedbacks,
        )
    }

    private fun uniquify(userId: String): String {
        return if (userId.startsWith("idPersona_")) {
            userId.split("-")[0] + "-" + serverId
        } else {
            userId
        }
    }

    private fun initServerId(): String {
        val file = File(SERVER_ID_FILE)
        var result: String
        if (file.exists()) {
            result = file.readText()
        } else {
            result = TokenHelper.newRandomToken().substring(0, 10)
            result = StringUtils.replaceChars(result, "+/", "-_")
            file.writeText(result)
        }
        return result
    }

    @GET
    @Path("personas")
    @Produces(MediaType.APPLICATION_JSON)
    fun getPersonas(): List<User> {
        logger.info("Persona Download called")
        return db.getPersonas()
    }

    companion object {
        private val logger = LoggerFactory.getLogger(HitchResource::class.java)
    }
}