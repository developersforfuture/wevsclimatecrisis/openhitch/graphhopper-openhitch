package de.wifaz.oh.server.photon

import de.wifaz.oh.protocol.OHPoint
import org.apache.commons.lang3.StringUtils
import org.slf4j.LoggerFactory
import java.lang.Double.isNaN
import javax.ws.rs.*
import javax.ws.rs.client.Client
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("hitch")
class HitchPhotonResource(
    private val photonUrl: String,
    private val photonReverseUrl: String,
    private val client: Client
) {

    @GET
    @Path("geocodeSearch")
    @Produces(MediaType.APPLICATION_JSON)
    fun photonSearch(
        @QueryParam("q") query: String,
        @QueryParam("lat") lat: Double?,
        @QueryParam("lon") lon: Double?,
        @QueryParam("lang") @DefaultValue("en") lang: String,
        @QueryParam("limit") @DefaultValue("5") limit: Int
    ): List<OHPoint> {
        validateQuery(query)
        validateCoordinates(lat, lon)
        val target = client.target(photonUrl)
            .queryParam("q", query)
            .queryParam("lat", lat)
            .queryParam("lon", lon)
            .queryParam("lang", getQueryLang(lang))
            .queryParam("limit", if (limit > 10) 10 else limit)
        return getOHPoints(target)
    }

    @GET
    @Path("geocodeReverseSearch")
    @Produces(MediaType.APPLICATION_JSON)
    fun photonReverserSearch(
        @QueryParam("lat") lat: Double?,
        @QueryParam("lon") lon: Double?,
        @QueryParam("locale") @DefaultValue("en") locale: String,
        @QueryParam("limit") @DefaultValue("1") limit: Int
    ): List<OHPoint> {
        validateCoordinates(lat, lon)
        val target = client.target(photonReverseUrl)
            .queryParam("lat", lat)
            .queryParam("lon", lon)
            .queryParam("lang", getQueryLang(locale))
            .queryParam("limit", if (limit > 10) 10 else limit)
        return getOHPoints(target)
    }

    private fun validateQuery(query: String) {
        if (query.isBlank()) {
            throw BadRequestException("q must not be empty")
        }
    }

    private fun validateCoordinates(lat: Double?, lon: Double?) {
        if (lat == null || lon == null || isNaN(lat) || isNaN(lon) || lon < -180 || lon > 180 || lat < -90 || lat > 90) {
            throw BadRequestException("lat and lon have to be valid coordinates")
        }
    }

    private fun getQueryLang(locale: String): String {
        return if (!SUPPORTED_LANGUAGES.contains(locale.toLowerCase())) "en" else locale.toLowerCase()
    }

    private fun getOHPoints(target: WebTarget): List<OHPoint> {
        target.request().accept(MediaType.APPLICATION_JSON).get().use { response ->
            if (!response.isSuccessful()) {
                LOGGER.warn("Photon request to ${target.uri} returned HTTP status ${response.status}")
                throw BadRequestException("Photon request to ${target.uri} returned HTTP status ${response.status}")
            }
            val photonResponse = response.readEntity(PhotonBackendResponse::class.java)
            return photonResponse.features.map { photonEntry ->
                OHPoint(
                    photonEntry.geometry.getLat() ?: 0.0,
                    photonEntry.geometry.getLon() ?: 0.0
                ).withDescription(getDescription(photonEntry.properties))
            }
        }
    }

    private fun getDescription(photonProperties: PhotonBackendResponse.PhotonEntry.PhotonProperties): String {
        return with(photonProperties) {
            StringBuilder()
                .appendIf(name.isNotEmpty(), name)
                .appendIf(street.isNotEmpty() && name.isNotEmpty() && name.notContains(street), ", ")
                .appendIf(street.isNotEmpty() && name.notContains(street), street)
                .appendIf(housenumber.isNotEmpty() && street.isNotEmpty(), " $housenumber")
                .appendIf(postcode.isNotEmpty() && (city.isNotEmpty() || state.isNotEmpty()), ", $postcode")
                .appendIf(city.isNotEmpty(), " $city")
                .appendIf(city.isEmpty() && state.isNotEmpty(), " $state")
                .toString()
        }
    }

    fun dispose() {
        client.close()
    }

    private fun Response.isSuccessful(): Boolean {
        return statusInfo.family == Response.Status.Family.SUCCESSFUL
    }

    private fun String?.isNotEmpty(): Boolean {
        return StringUtils.isNotEmpty(this)
    }

    private fun String?.notContains(str: String?): Boolean {
        return !StringUtils.contains(this, str)
    }

    private fun String?.isEmpty(): Boolean {
        return StringUtils.isEmpty(this)
    }

    private fun StringBuilder.appendIf(predicate: Boolean, str: String?): StringBuilder {
        if (predicate) {
            append(str)
        }
        return this
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(HitchPhotonResource::class.java)

        // Languages supported by photon, see https://github.com/komoot/photon
        private val SUPPORTED_LANGUAGES = setOf("en", "de", "fr")
    }

}