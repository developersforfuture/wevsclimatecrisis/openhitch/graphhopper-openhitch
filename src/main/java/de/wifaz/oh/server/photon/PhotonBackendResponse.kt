package de.wifaz.oh.server.photon

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class PhotonBackendResponse(
    @JsonProperty("features")
    var features: List<PhotonEntry> = mutableListOf()
) {
    @JsonIgnoreProperties(ignoreUnknown = true)
    data class PhotonEntry(
        @JsonProperty("geometry")
        var geometry: GeojsonPoint,

        @JsonProperty("properties")
        var properties: PhotonProperties
    ) {

        @JsonIgnoreProperties(ignoreUnknown = true)
        data class GeojsonPoint(
            @JsonProperty("coordinates")
            var coordinates: List<Double>,

            @JsonProperty("type")
            var type: String = "Point"
        ) {

            fun getLat(): Double? {
                return if (ensureGeometryCapacity()) coordinates[1] else null
            }

            fun getLon(): Double? {
                return if (ensureGeometryCapacity()) coordinates[0] else null
            }

            private fun ensureGeometryCapacity(): Boolean {
                return coordinates.size == 2 && type == "Point"
            }
        }

        @JsonIgnoreProperties(ignoreUnknown = true)
        data class PhotonProperties(
            @JsonProperty("osm_id")
            var osmId: Long = 0,

            @JsonProperty("osm_type")
            var osmType: String?,

            @JsonProperty("name")
            var name: String?,

            @JsonProperty("osm_key")
            var osmKey: String?,

            @JsonProperty("osm_value")
            var osmValue: String?,

            @JsonProperty("country")
            var country: String?,

            @JsonProperty("state")
            var state: String?,

            @JsonProperty("city")
            var city: String?,

            @JsonProperty("street")
            var street: String?,

            @JsonProperty("housenumber")
            var housenumber: String?,

            @JsonProperty("postcode")
            var postcode: String?,

            @JsonProperty("extent")
            var extent: List<Double>?
        )
    }
}