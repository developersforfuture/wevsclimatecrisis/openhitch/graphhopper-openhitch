package de.wifaz.oh.server

import com.google.common.hash.Hashing
import java.security.SecureRandom
import java.util.*

object TokenHelper {

    /**
     * The number of bits used in tokens created by this class
     */
    private const val TOKEN_SIZE_BITS = 256

    /**
     * The number of bytes used in tokens created by this class.
     */
    private const val TOKEN_SIZE_BYTES = TOKEN_SIZE_BITS / Byte.SIZE_BITS

    private val random = SecureRandom()

    /**
     * Creates a new random token as a Base64 encoded string.
     */
    fun newRandomToken(): String {
        return toBase64(random.nextBytes(TOKEN_SIZE_BYTES))
    }

    /**
     * Returns the Base64 encoded hash of the given token. The returned string is suitable to be stored in the DB.
     */
    fun asDbHash(token: ByteArray): String {
        return toBase64(hashToken(token))
    }

    /**
     * Returns the Base64 encoded hash of the given token that itself is Base64 encoded. The returned string is suitable
     * to be stored in the DB.
     */
    fun asDbHash(base64Token: String): String {
        return toBase64(hashToken(fromBase64(base64Token)))
    }

    /**
     * Creates a SHA-256 hash for the given token.
     */
    private fun hashToken(token: ByteArray): ByteArray {
        return Hashing.sha256().hashBytes(token).asBytes()
    }

    /**
     * Helper function that decodes the given Base64 string to a byte array.
     */
    private fun fromBase64(base64String: String): ByteArray {
        return Base64.getDecoder().decode(base64String)
    }

    /**
     * Helper function that encodes the given bytes as a Base64 string.
     */
    private fun toBase64(bytes: ByteArray): String {
        return Base64.getEncoder().encodeToString(bytes)
    }

    /**
     * Extension function on SecureRandom to create the specified number of random bytes.
     */
    private fun SecureRandom.nextBytes(count: Int): ByteArray {
        val bytes = ByteArray(count)
        this.nextBytes(bytes)
        return bytes
    }
}
