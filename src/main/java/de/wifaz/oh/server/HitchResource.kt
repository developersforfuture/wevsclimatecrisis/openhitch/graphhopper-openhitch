/*
 * (c) Max kubierschky
 *
 * Max kubierschky licenses this file to you under the Server Side Public License,
 *  Version 1 (the "License"); you may not use this file except in
 *  compliance with the License. You may obtain a copy of the License at
 *
 *       https://www.mongodb.com/licensing/server-side-public-license
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
/*
 * (c) Max Kubierschky
 *
 * Max Kubierschky licenses this file to you under the Server Side Public License,
 *  Version 1 (the "License"); you may not use this file except in
 *  compliance with the License. You may obtain a copy of the License at
 *
 *       https://www.mongodb.com/licensing/server-side-public-license
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.wifaz.oh.server

import de.wifaz.oh.protocol.*
import de.wifaz.oh.protocol.Lift.Status.*
import de.wifaz.oh.protocol.Role.DRIVER
import de.wifaz.oh.protocol.Role.PASSENGER
import org.apache.commons.io.IOUtils
import org.slf4j.LoggerFactory
import java.io.InputStream
import java.net.HttpURLConnection
import java.util.*
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.*

/**
 * Maximum size for a profile picture is 512 KB.ĸ
 */
private const val MAX_PROFILE_PICTURE_SIZE = 1024 * 512

@Path("hitch")
class HitchResource @Inject constructor(
    private val db: HitchDB,
    private val messenger: Messenger,
    private val matcher: Matcher
) {

    private val timeMatcher = TimeMatcher()

    /**
     * FIXME add restrictions on amount of createUser calls
     */
    @POST
    @Path("users/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun createUser(user: User): CreateUserResponse {
        return db.transaction {
            val userId = user.id
            if (db.hasUser(userId)) {
                logger.error("Invalid createUser request, duplicate user id: $user")
                throw WebApplicationException(
                    "Invalid create request, duplicate user id: $userId",
                    HttpURLConnection.HTTP_CONFLICT
                )
            }
            logger.info("create: $user")
            db.putUser(user)
            val token = TokenHelper.newRandomToken()
            db.putToken(user.id, TokenHelper.asDbHash(token))
            val rs = RegistrationState(
                user_id = userId,
                has_profile_picture = false,
                phone_verified = false,
                fully_registered = false
            )
            db.putRegistrationState(rs)
            return@transaction CreateUserResponse(token, rs)
        }
    }

    @PUT
    @Secured
    @Path("user/{user_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    fun updateUser(
        @PathParam("user_id") user_id: String,
        user: User,
        @Context securityContext: SecurityContext
    ): Response {
        return db.transaction {
            checkUser(user_id, securityContext)
            if (user_id != user.id) {
                throw WebApplicationException("updateUser: Inconsistent user id's", HttpURLConnection.HTTP_CONFLICT)
            }
            if (!db.hasUser(user_id)) {
                throw WebApplicationException(
                    "updateUser: Invalid request, update on non existing user id: $user_id",
                    HttpURLConnection.HTTP_CONFLICT
                )
            }
            logger.info("update: $user")
            db.putUser(user)
            return@transaction Response.ok().build()
        }
    }

    @DELETE
    @Secured
    @Path("user/{user_id}")
    fun deleteUser(@PathParam("user_id") user_id: String, @Context securityContext: SecurityContext): Response {
        return db.transaction {
            checkUser(user_id, securityContext, mustBeFullyRegistered = false)
            try {
                if (!db.hasUser(user_id)) {
                    throw WebApplicationException(
                        "Invalid delete request, user id: $user_id does not exist",
                        HttpURLConnection.HTTP_CONFLICT
                    )
                }

                logger.info("cancel: $user_id")
                db.removeUser(user_id)
                return@transaction Response.ok().build()
            } catch (e: IllegalArgumentException) {
                throw WebApplicationException(HttpURLConnection.HTTP_BAD_REQUEST)
            }
        }
    }

    private fun checkUser(userId: String, securityContext: SecurityContext, mustBeFullyRegistered: Boolean = true) {
        val username = securityContext.userPrincipal.name
        logger.debug("username=$username")
        if (userId != username) {
            throw error(Response.Status.FORBIDDEN)
        }
        if (mustBeFullyRegistered && !db.getRegistrationState(userId).fully_registered) {
            throw error(Response.Status.FORBIDDEN, "user $userId is not fully registered")
        }
    }

    @PUT
    @Secured
    @Path("feedbacks/create")
    @Consumes(MediaType.APPLICATION_JSON)
    fun createFeedback(feedback: Feedback, @Context securityContext: SecurityContext): Response {
        checkUser(feedback.user_id, securityContext)
        db.putFeedback(feedback)
        return Response.ok().build()
    }

    private fun error(status: Response.Status, message: String? = null): WebApplicationException {
        val exception = WebApplicationException(message, status.statusCode)
        logger.error(message ?: status.reasonPhrase, exception)
        return exception
    }

    @PUT
    @Secured
    @Path("ways/create")
    @Consumes(MediaType.APPLICATION_JSON)
    fun createWay(way: Way, @Context securityContext: SecurityContext): Response {
        // FIXME brauche Eingangsprüfung für Weg
            return db.transaction {
            checkUser(way.user_id, securityContext)
            val wayId = way.id
            if (db.hasWay(wayId)) {
                logger.error("Invalid create request, duplicate way id: $way")
                throw WebApplicationException(
                    "Invalid create request, duplicate way id: $way",
                    HttpURLConnection.HTTP_CONFLICT
                )
            }
            logger.info("create: $way")
            db.putWay(way)
            match(way)
            return@transaction Response.ok().build()
        }
    }

    private fun match(way: Way) { // FIXME: perform asynchronously
        if (way.waypoints.size != 2) {
            logger.error("way with stopover (way_id=" + way.id + "). Not yet implemented")
            // FIXME implement for drivers, block for passengers
            return
        }
        when (way.role) {
            PASSENGER -> matchPassenger(way)
            DRIVER -> matchDriver(way)
        }
    }

    private fun matchDriver(wayDriver: Way) {
        for (wayPassenger in db.getAllWays()) {
            if (wayPassenger.role != PASSENGER) {
                continue
            }
            if (wayPassenger.waypoints.size != 2) {
                logger.error("Passenger way with stopover (way_id=" + wayPassenger.id + ").")
                continue
            }
            var lift = match(wayPassenger, wayDriver) ?: return
            val bestMatches = db.getSuggestedLiftsOrderedByRating(wayPassenger.id)
            val goodEnough = if (bestMatches.size < HitchInterface.MAX_OFFERS) {
                true
            } else if (lift.rating > bestMatches.last().rating) {
                db.removeLift(bestMatches.last())
                true
            } else {
                false
            }
            if (goodEnough) {
                if (wayPassenger.autocommit && !lift.sectional && !db.isEngaged(wayPassenger.id)) {
                    val liftStatus = when (wayDriver.status) {
                        Way.Status.RESEARCH -> PREVIEW
                        else -> REQUESTED
                    }
                    lift = lift.copy(status = liftStatus)
                    db.putLift(lift)
                    val wayDriver = db.getWay(lift.driver_way_id)
                    val driverId = wayDriver.user_id
                    sendLiftInfoToDriver(driverId, lift, wayPassenger)
                    if (liftStatus == REQUESTED) {
                        sendLiftInfoToPassenger(wayPassenger.user_id, lift, wayDriver)
                    }
                } else {
                    db.putLift(lift)
                    sendLiftInfoToPassenger(wayPassenger.user_id, lift, wayDriver)
                }
            }
        }
    }

    private fun sendLiftInfoToPassenger(passengerId: String, lift: Lift, wayDriver: Way) {
        val driver = db.getUser(wayDriver.user_id)
        val offer = LiftInfo(
            lift = lift,
            role = PASSENGER,
            partner = driver,
            partner_way = wayDriver
        )
        val message = LiftInfoMessage(lift.passenger_way_id, ArrayList(setOf(offer)))
        messenger.send(passengerId, message)
    }

    private fun matchPassenger(wayPassenger: Way) {
        val comparator = kotlin.Comparator<Lift>({ a, b -> -java.lang.Double.compare(a.rating, b.rating) })
        val bestMatches =
            HighscoreList<Lift>(HitchInterface.MAX_OFFERS, comparator )
        for (wayDriver in db.getAllWays()) {
            if (wayDriver.role != Role.DRIVER) {
                continue
            }
            if (wayDriver.status == Way.Status.RESEARCH && !wayPassenger.autocommit) {
                continue
            }
            if (wayDriver.waypoints.size != 2) {
                logger.error("Way with stopover (way_id=" + wayDriver.id + "). Not yet implemented")
                // FIXME implement
                continue
            }
            var lift = match(wayPassenger, wayDriver) ?: continue
            if (wayPassenger.autocommit && !lift.sectional) {
                val liftStatus = when (wayDriver.status) {
                    Way.Status.RESEARCH -> PREVIEW
                    else -> REQUESTED
                }
                lift = lift.copy(status = liftStatus)
            }
            bestMatches.add(lift)
        }
        var lifts = ArrayList(bestMatches)
        for (lift in lifts) {
            db.putLift(lift)
        }
        if (wayPassenger.autocommit) {
            for (lift in lifts) {
                if (lift.status in EnumSet.of(REQUESTED, PREVIEW)) {
                    val driverId = db.getWay(lift.driver_way_id).user_id
                    sendLiftInfoToDriver(driverId, lift, wayPassenger)
                }
            }
        }
        val liftsReferredToPassenger = lifts.filter { it.status != PREVIEW }
        // Note: We send a message in any case. If we didn't find anything, we send an empty list
        // such that the client can inform the user about the zero result.
        val message = createLiftOfferMessage(wayPassenger.id, liftsReferredToPassenger)
        messenger.send(wayPassenger.user_id, message)
    }

    private fun sendLiftInfoToDriver(driverId: String, lift: Lift, wayPassenger: Way) {
        val liftInfo = LiftInfo(
            lift,
            role = DRIVER,
            partner_way = wayPassenger,
            partner = db.getUser(wayPassenger.user_id)
        )
        val message = LiftInfoMessage(lift.driver_way_id, ArrayList(setOf(liftInfo)))
        messenger.send(driverId, message)
    }

    private fun match(wayPassenger: Way, wayDriver: Way): Lift? {
        if (wayPassenger.user_id == wayDriver.user_id) {
            return null
        }
        if (!timeMatcher.preMatch(wayPassenger, wayDriver)) {
            return null
        }
        matcher.match(wayPassenger, wayDriver)?.let {
            if (timeMatcher.postMatch(it, wayPassenger, wayDriver)) {
                return it
            }
        }
        return null
    }

    private fun createLiftOfferMessage(passenger_way_id: String, bestMatches: List<Lift>): HitchMessage {
        val offers = ArrayList<LiftInfo>()
        for (lift in bestMatches) {
            val driverWay = db.getWay(lift.driver_way_id)
            val driver = db.getUser(driverWay.user_id)
            val liftInfo = LiftInfo(
                lift = lift,
                role = PASSENGER,
                partner = driver,
                partner_way = driverWay
            )
            offers.add(liftInfo)
        }
        return LiftInfoMessage(passenger_way_id, offers)
    }

    @DELETE
    @Secured
    @Path("way/{wayId}")
    fun deleteWay(@PathParam("wayId") wayId: String, @Context securityContext: SecurityContext): Response {
        return db.transaction {
            try {
                if (!db.hasWay(wayId)) {
                    throw WebApplicationException(
                        "Invalid cancel request, way_id not listed: $wayId",
                        HttpURLConnection.HTTP_BAD_REQUEST
                    )
                }
                val userId = db.getWay(wayId).user_id
                checkUser(userId, securityContext)
                logger.info("cancel: $wayId")
                cancelAllLifts(wayId)
                db.removeWay(wayId)
                return@transaction Response.ok().build()
            } catch (e: IllegalArgumentException) {
                throw WebApplicationException(HttpURLConnection.HTTP_BAD_REQUEST)
            }
        }
    }

    private fun cancelAllLifts(wayId: String) {
        val way = db.getWay(wayId)
        val role = way.role
        for (lift in db.getLifts(wayId)) {
            val isNotificationNeeded = when (lift.status) {
                SUGGESTED -> role==DRIVER
                PREVIEW -> role==PASSENGER
                REQUESTED, ACCEPTED, REJECTED, BOARDED -> true
                FINISHED, DRIVER_CANCELED, PASSENGER_CANCELED, WITHDRAWN -> false
            }.exhaustive
            if (isNotificationNeeded) {
                val newStatus = when (role) {
                    DRIVER -> DRIVER_CANCELED
                    PASSENGER -> PASSENGER_CANCELED
                }
                val newLift = lift.copy(status = newStatus)
                informPartnerOfLiftStatusChange(newLift, lift.status, way.user_id, role)
            }
        }
    }

    private fun getRole(lift: Lift, user_id: String): Role? {
        val passengerWay = db.getWay(lift.passenger_way_id)
        if (passengerWay.user_id == user_id) {
            return PASSENGER
        }
        val driverWay = db.getWay(lift.driver_way_id)
        if (driverWay.user_id == user_id) {
            return DRIVER
        }
        return null
    }

    @PUT
    @Secured
    @Path("way/{way_id}/status")
    @Consumes(MediaType.APPLICATION_JSON)
    fun updateWayStatus(
        @PathParam("way_id") way_id: String,
        newStatus: Way.Status,
        @Context securityContext: SecurityContext
    ): Response {
        return db.transaction {
            if (!db.hasWay(way_id)) {
                throw WebApplicationException(
                    "Invalid update request, way_id not listed: $way_id",
                    HttpURLConnection.HTTP_BAD_REQUEST
                )
            }
            val way = db.getWay(way_id)
            checkUser(way.user_id, securityContext)

            val ok = verifyWayStatusChange(way.status, newStatus)
            if (!ok) {
                val message = String.format(
                    "Invalid update request, status transition not allowed: %s, %s",
                    way.status.name,
                    newStatus.name
                )
                throw WebApplicationException(message, HttpURLConnection.HTTP_FORBIDDEN)
            }

            val newWay = way.copy(status = newStatus)
            db.putWay(newWay)
            // FIXME the app is not yte ready to receive this message
            // informPartnerOfWayStatusChange(newWay)
            return@transaction Response.ok().build()
        }
    }

    private fun verifyWayStatusChange(old: Way.Status, new: Way.Status): Boolean {
        return (old == Way.Status.RESEARCH && new == Way.Status.PUBLISHED)
                || (old == Way.Status.RESEARCH && new == Way.Status.STARTED)
                || (old == Way.Status.PUBLISHED && new == Way.Status.STARTED)
                || (old == Way.Status.STARTED && new == Way.Status.FINISHED)
                || (old == Way.Status.PUBLISHED && new == Way.Status.CANCELED)
                || (old == Way.Status.STARTED && new == Way.Status.CANCELED)
    }

    @PUT
    @Secured
    @Path("lift/{lift_id}/status")
    @Consumes(MediaType.APPLICATION_JSON)
    fun updateLiftStatus(
        @PathParam("lift_id") lift_id: String,
        newStatus: Lift.Status,
        @Context securityContext: SecurityContext
    ): Response {
        return db.transaction {
            val username = securityContext.userPrincipal.name
            logger.debug("username=$username")
            if (!db.hasLift(lift_id)) {
                throw WebApplicationException(
                    "Invalid update request, lift_id not listed: $lift_id",
                    HttpURLConnection.HTTP_BAD_REQUEST
                )
            }
            val lift = db.getLift(lift_id)
            if (newStatus == ACCEPTED && db.isEngaged(lift.passenger_way_id)) {
                val message = "Transition to status ACCEPTED failed, passenger is already engaged: lift=${lift.id}"
                throw WebApplicationException(message, HttpURLConnection.HTTP_FORBIDDEN)
            }
            val oldStatus = lift.status
            val role = getRole(lift, username)
                ?: throw WebApplicationException(
                    "Invalid update request, lift doesn't involve user: lift_id=$lift_id, user_id=$username",
                    HttpURLConnection.HTTP_BAD_REQUEST
                )
            val ok = (oldStatus == SUGGESTED && newStatus == REQUESTED && role == PASSENGER
                    || oldStatus == REQUESTED && newStatus == ACCEPTED && role == DRIVER
                    || oldStatus == REQUESTED && newStatus == REJECTED && role == DRIVER
                    || oldStatus == PREVIEW && newStatus == ACCEPTED && role == DRIVER
                    || oldStatus == ACCEPTED && newStatus == PASSENGER_CANCELED && role == PASSENGER
                    || oldStatus == ACCEPTED && newStatus == DRIVER_CANCELED && role == DRIVER)
            if (ok) {
                val newLift = lift.copy(status = newStatus)
                db.putLift(newLift)
                informPartnerOfLiftStatusChange(newLift, oldStatus, username, role)
                if (newStatus == ACCEPTED) {
                    unrequestOtherLifts(lift)
                }
                return@transaction Response.ok().build()
            } else {
                val message = String.format(
                    "Invalid update request, status transition not allowed: %s, %s, %s",
                    oldStatus.name,
                    newStatus.name,
                    role.name
                )
                throw WebApplicationException(message, HttpURLConnection.HTTP_FORBIDDEN)
            }
        }
    }

    private fun unrequestOtherLifts(acceptedLift: Lift) {
        val passengerWay = db.getWay(acceptedLift.passenger_way_id)
        val driverWay = db.getWay(acceptedLift.driver_way_id)
        for (lift in db.getLifts(acceptedLift.passenger_way_id)) {
            if (lift.id == acceptedLift.id) {
                continue
            }
            val newLift = lift.copy(status = WITHDRAWN)
            when (lift.status) {
                REQUESTED, PREVIEW -> {
                    informDriverOfLiftStatusChange(newLift, lift.status, passengerWay.user_id)
                }
                ACCEPTED, BOARDED, FINISHED -> {
                    throw WebApplicationException("Internal error", HttpURLConnection.HTTP_INTERNAL_ERROR)
                }
                SUGGESTED, REJECTED, DRIVER_CANCELED, PASSENGER_CANCELED, WITHDRAWN -> {
                }
            }.exhaustive
            informPassengerOfLiftStatusChange(newLift, lift.status, driverWay.user_id)
        }
    }

    private fun informPartnerOfWayStatusChange(way: Way) {
        for (lift in db.getLifts(way.id)) {
            when (lift.status) {
                SUGGESTED, PREVIEW, WITHDRAWN, REJECTED, FINISHED, DRIVER_CANCELED, PASSENGER_CANCELED -> {
                    //ignore
                }
                BOARDED, REQUESTED, ACCEPTED -> {
                    val message = WayStatusMessage(
                        way_id = way.id,
                        status = way.status
                    )
                    val partnerId: String = getPartnerId(way, lift)
                    messenger.send(partnerId, message)
                }
            }.exhaustive
        }
    }

    private fun getPartnerId(way: Way, lift: Lift): String {
        return when (way.role) {
            DRIVER -> db.getWay(lift.passenger_way_id).user_id
            PASSENGER -> db.getWay(lift.driver_way_id).user_id
        }
    }

    private fun informDriverOfLiftStatusChange(newLift: Lift, oldStatus: Lift.Status, passengerId: String) {
        val driverId = db.getWay(newLift.driver_way_id).user_id
        if (oldStatus == SUGGESTED) {
            val liftInfo = LiftInfo(
                newLift,
                role = DRIVER,
                partner_way = db.getWay(newLift.passenger_way_id),
                partner = db.getUser(passengerId)
            )
            val message = LiftInfoMessage(newLift.driver_way_id, ArrayList(setOf(liftInfo)))
            messenger.send(driverId, message)
        } else {
            val message = LiftStatusMessage(
                reference_way_id = newLift.driver_way_id,
                lift_id = newLift.id,
                status = newLift.status
            )
            messenger.send(driverId, message)
        }
    }

    private fun informPassengerOfLiftStatusChange(newLift: Lift, oldStatus: Lift.Status, driverId: String) {
        val passengerId = db.getWay(newLift.passenger_way_id).user_id
        val message = LiftStatusMessage(
            reference_way_id = newLift.passenger_way_id,
            lift_id = newLift.id,
            status = newLift.status
        )
        messenger.send(passengerId, message)
    }

    private fun informPartnerOfLiftStatusChange(
        newLift: Lift,
        oldStatus: Lift.Status,
        issuerId: String,
        issuerRole: Role
    ) {
        when (issuerRole) {
            PASSENGER -> {
                val driverId = db.getWay(newLift.driver_way_id).user_id
                if (oldStatus == SUGGESTED) {
                    val liftInfo = LiftInfo(
                        newLift,
                        role = DRIVER,
                        partner_way = db.getWay(newLift.passenger_way_id),
                        partner = db.getUser(issuerId)
                    )
                    val message = LiftInfoMessage(newLift.driver_way_id, ArrayList(setOf(liftInfo)))
                    messenger.send(driverId, message)
                } else {
                    val message = LiftStatusMessage(
                        reference_way_id = newLift.driver_way_id,
                        lift_id = newLift.id,
                        status = newLift.status
                    )
                    messenger.send(driverId, message)
                }
            }
            DRIVER -> {
                val passengerId = db.getWay(newLift.passenger_way_id).user_id
                if (oldStatus == PREVIEW) {
                    val liftInfo = LiftInfo(
                        newLift,
                        role = PASSENGER,
                        partner_way = db.getWay(newLift.driver_way_id),
                        partner = db.getUser(issuerId)
                    )
                    val message = LiftInfoMessage(newLift.passenger_way_id, ArrayList(setOf(liftInfo)))
                    messenger.send(passengerId, message)
                } else {
                    val message = LiftStatusMessage(
                        reference_way_id = newLift.passenger_way_id,
                        lift_id = newLift.id,
                        status = newLift.status
                    )
                    messenger.send(passengerId, message)
                }
            }
        }
    }


    @GET
    @Secured
    @Path("syncData/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getSyncData(@PathParam("userId") userId: String, @Context securityContext: SecurityContext): SyncData {
        logger.debug("getUserData $userId")
        checkUser(userId, securityContext)
        val user = db.getUser(userId)
        val ways = db.getWaysForUser(userId)
        val liftInfos = ArrayList<LiftInfo>()
        for (way in ways) {
            for (lift in db.getLifts(way.id)) {
                try {
                    val role = getRole(lift, userId)
                    val partnerWay = when (role) {
                        DRIVER -> db.getWay(lift.passenger_way_id)
                        PASSENGER -> db.getWay(lift.driver_way_id)
                        null -> throw HitchDBException("Database lists unrelated way ${way.id} for user $userId")
                    }
                    val partner = db.getUser(partnerWay.user_id)
                    val liftInfo = LiftInfo(lift, role, partner, partnerWay)
                    liftInfos.add(liftInfo)
                } catch (e: HitchDBException) {
                    logger.error("Database integrity error for user $userId", e)
                    continue
                }
            }
        }

        return SyncData(user, ArrayList(ways), ArrayList(liftInfos))
    }

    @PUT
    @Secured
    @Path("user/{user_id}/profilePicture")
    @Consumes("image/jpeg")
    @Produces(MediaType.APPLICATION_JSON)
    fun updateProfilePicture(
        @PathParam("user_id") userId: String,
        request: InputStream,
        @HeaderParam(HttpHeaders.CONTENT_LENGTH) contentLength: Long?,
        @Context securityContext: SecurityContext
    ): RegistrationState {
        return db.transaction {
            checkUser(userId, securityContext, false)
            if (contentLength == null || contentLength < 1) {
                throw error(Response.Status.LENGTH_REQUIRED)
            }
            if (contentLength > MAX_PROFILE_PICTURE_SIZE) {
                throw error(Response.Status.NOT_ACCEPTABLE)
            }
            // To prevent out-of-memory errors with malicious requests we read at most contentLength bytes in case the
            // request body contains more data than declared. Thus we don't want to use request.readBytes() here.
            val profilePicture = ByteArray(contentLength.toInt())
            IOUtils.read(request, profilePicture, 0, contentLength.toInt())
            db.putProfilePicture(userId, profilePicture)
            val rs = db.getRegistrationState(userId).copy(has_profile_picture = true, fully_registered = true)
            db.putRegistrationState(rs)
            return@transaction rs
        }
    }

    @GET
    @Path("user/{user_id}/profilePicture")
    @Produces("image/jpeg")
    fun getProfilePicture(@PathParam("user_id") userId: String): Response {
        if (!db.hasProfilePicture(userId)) {
            return Response.status(Response.Status.NOT_FOUND).build()
        }
        return Response.ok(db.getProfilePicture(userId)).build()
    }

    companion object {
        private val logger = LoggerFactory.getLogger(HitchResource::class.java)
    }
}