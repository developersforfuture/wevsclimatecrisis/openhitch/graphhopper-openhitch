package de.wifaz.oh.server

import com.mongodb.client.ClientSession
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.CountOptions
import de.wifaz.oh.protocol.*
import org.bson.BsonBinary
import org.bson.BsonDocument
import org.litote.kmongo.*
import org.litote.kmongo.util.KMongoUtil
import org.litote.kmongo.util.idValue
import java.util.*
import kotlin.collections.ArrayList

// TODO: performance: are there indexes in MongoDB? We need on on every id that is searched for
// TODO: Can we enforce uniqueness?

private const val DOCUMENT_PROPERTY_PICTURE = "profilePicture"


class MongoHitchDB(val name: String, val hook: HitchDBHook) : HitchDB {

    // The database can be accessed directly for testing purposes.
    // In production  code it is only accessed through the HitchDB interface
    val client: MongoClient
    val database: MongoDatabase

    private val users: MongoCollection<User>
    private val userTokens: MongoCollection<UserToken>
    private val registrationStates: MongoCollection<RegistrationState>
    private val ways: MongoCollection<Way>
    private val lifts: MongoCollection<Lift>
    private val profilePictures: MongoCollection<BsonDocument>
    private val feedbacks: MongoCollection<Feedback>

    init {
        client = KMongo.createClient()
        database = client.getDatabase(name)
        ways = database.getCollection<Way>()
        users = database.getCollection<User>()
        feedbacks = database.getCollection<Feedback>()
        userTokens = database.getCollection<UserToken>()
        profilePictures = database.getCollection("profilePicture", BsonDocument::class.java)
        lifts = database.getCollection<Lift>()
        registrationStates = database.getCollection<RegistrationState>()
    }

    override fun <T>transaction(code: () -> T) : T {
        var session = client.startSession()
        return session.withTransaction(code)
    }

    override fun putUser(user: User) {
        users.updateOneById(user.id, user, upsert())
    }

    override fun hasUser(userId: String): Boolean {
        return users.hasOneById(userId)
    }

    override fun getUser(userId: String): User {
        return findUser(userId) ?: throw HitchDBException("Reference to unknown user $userId")
    }

    private fun findUser(userId: String): User? {
        return users.findOneById(userId)
    }

    override fun removeUser(userId: String) {
        for (way in ways.find(Way::user_id eq userId)) {
            removeWay(way.id)
        }
        users.deleteOneById(userId)
        profilePictures.deleteOneById(userId)
        userTokens.deleteMany(UserToken::userId eq userId)
    }

    override fun getPersonas(): List<User> {
        return users.find(User::id regex "^idPersona_.*").toList()
    }

    override fun getProfilePicture(userId: String): ByteArray? {
        val pictureDocument = profilePictures.findOneById(userId)?.getDocument(DOCUMENT_PROPERTY_PICTURE)
        return pictureDocument?.getBinaryData()
    }

    override fun putProfilePicture(userId: String, profilePicture: ByteArray) {
        val profilePicture = BsonDocument(DOCUMENT_PROPERTY_PICTURE, BsonBinary(profilePicture))
        profilePictures.replaceOneById(userId, profilePicture, replaceUpsert())
    }

    override fun hasProfilePicture(userId: String): Boolean {
        return profilePictures.hasOneById(userId)
    }

    override fun getFeedback(feedbackId: String): Feedback {
        return feedbacks.findOneById(feedbackId) ?: throw HitchDBException("Reference to unknown feedback $feedbackId")
    }

    override fun putFeedback(feedback: Feedback) {
        feedbacks.updateOneById(feedback.id, feedback, upsert())
    }

    override fun putToken(userId: String, token: String) {
        userTokens.updateOneById(token, UserToken(userId, token), upsert())
    }

    override fun hasToken(userId: String, token: String): Boolean {
        if (!users.hasOneById(userId)) throw HitchDBException("Reference to unknown user $userId")
        return userTokens.findOne(and(UserToken::userId eq userId, UserToken::token eq token)) != null
    }

    override fun getTokenUser(token: String): String? {
        val userToken = userTokens.findOne(UserToken::token eq token)
        return userToken?.userId
    }

    override fun removeToken(userId: String, token: String) {
        userTokens.deleteOne(and(UserToken::userId eq userId, UserToken::token eq token))
    }

    override fun putRegistrationState(registrationState: RegistrationState) {
        registrationStates.updateOneById(registrationState.user_id, registrationState, upsert())
    }

    override fun getRegistrationState(userId: String): RegistrationState {
        val result = registrationStates.findOneById(userId)
        return result ?: throw HitchDBException("RegistrationState not found for user $userId")
    }

    override fun getLift(liftId: String): Lift {
        return findLift(liftId) ?: throw HitchDBException("Reference to unknown lift $liftId")

    }

    private fun findLift(liftId: String): Lift? {
        return lifts.findOneById(liftId)
    }

    override fun hasLift(liftId: String): Boolean {
        return lifts.hasOneById(liftId)
    }

    override fun isEngaged(wayId: String): Boolean {
        val lift = lifts.findOne(Lift::passenger_way_id eq wayId, Lift::status `in` Lift.engagedStates)
        return lift != null

    }

    override fun putLift(lift: Lift) {
        // TODO ensure referential integrity
        lifts.updateOneById(lift.id, lift, upsert())
    }

    override fun removeLift(lift: Lift) {
        hook.onRemoveLift(this, lift)
        lifts.deleteOneById(lift.id)
    }

    override fun putWay(way: Way) {
        if (!users.hasOneById(way.user_id)) throw HitchDBException("Reference to unknown user ${way.user_id}")
        ways.updateOneById(way.id, way, upsert())
    }

    override fun hasWay(wayId: String): Boolean {
        return ways.hasOneById(wayId)
    }

    override fun removeWay(wayId: String) {
        val condition = or(Lift::driver_way_id eq wayId, Lift::passenger_way_id eq wayId)
        for (lift in lifts.find(condition)) {
            removeLift(lift)
        }
        ways.deleteOneById(wayId)
    }

    override fun getAllWays(): List<Way> {
        return ways.find().into(ArrayList())
    }

    override fun getWay(wayId: String): Way {
        return findWay(wayId) ?: throw HitchDBException("Reference to unknown way $wayId")
    }

    private fun findWay(wayId: String): Way? {
        return ways.findOneById(wayId)
    }

    override fun getLifts(wayId: String): List<Lift> {
        val way = getWay(wayId)
        return when (way.role) {
            Role.DRIVER -> lifts.find(Lift::driver_way_id eq wayId).toList()
            Role.PASSENGER -> lifts.find(Lift::passenger_way_id eq wayId).toList()
        }
    }

    override fun getSuggestedLiftsOrderedByRating(wayId: String): List<Lift> {
        val bson = and(Lift::passenger_way_id eq wayId, Lift::status eq Lift.Status.SUGGESTED)
        return lifts.find(bson).sort(descending(Lift::rating)).toList()
    }

    override fun getWaysForUser(userId: String?): List<Way> {
        return ways.find(Way::user_id eq userId).toList()
    }

    override fun dump(): DBDump {
        return DBDump(
            time = Date().time,
            users = users.find().into(ArrayList()),
            user_tokens = userTokens.find().into(ArrayList()),
            registration_states = registrationStates.find().into(ArrayList()),
            ways = ways.find().into(ArrayList()),
            lifts = lifts.find().into(ArrayList()),
            profile_pictures = profilePictures.find()
                .map { Pair(it.idValue as String, Base64.getEncoder().encodeToString(it.getBinaryData())) }.toMap(),
            feedbacks = feedbacks.find().into(ArrayList())
        )
    }

    override fun restore(dbDump: DBDump) {
        database.drop()
        dbDump.users.forEach { putUser(it) }
        dbDump.user_tokens.forEach { putToken(it.userId, it.token) }
        dbDump.ways.forEach { putWay(it) }
        dbDump.lifts.forEach { putLift(it) }
        dbDump.profile_pictures.forEach { putProfilePicture(it.key, Base64.getDecoder().decode(it.value)) }
        dbDump.feedbacks.forEach { putFeedback(it) }
    }

    override fun isClean(): Boolean {
        return users.estimatedDocumentCount() == 0L
                && userTokens.estimatedDocumentCount() == 0L
                && ways.estimatedDocumentCount() == 0L
                && lifts.estimatedDocumentCount() == 0L
                && profilePictures.estimatedDocumentCount() == 0L
                && feedbacks.estimatedDocumentCount() == 0L
    }

    fun close() {
        client.close()
    }

    private fun BsonDocument.getBinaryData(): ByteArray {
        return this.getBinary("data").data
    }

    private fun <T> MongoCollection<T>.hasOneById(id: Any): Boolean {
        return this.countDocuments(KMongoUtil.idFilterQuery(id), CountOptions().limit(1)) == 1L
    }
}