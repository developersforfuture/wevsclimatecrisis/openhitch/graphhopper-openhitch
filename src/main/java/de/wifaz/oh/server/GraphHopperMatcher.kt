package de.wifaz.oh.server

import com.graphhopper.GHRequest
import com.graphhopper.GraphHopperAPI
import com.graphhopper.ResponsePath
import com.graphhopper.util.Parameters
import com.graphhopper.util.PointList
import com.graphhopper.util.shapes.GHPoint
import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.OHPoint
import de.wifaz.oh.protocol.Way
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject

private val logger = LoggerFactory.getLogger(GraphHopperMatcher::class.java)
private const val MAX_DETOUR_DISTANCE_RATIO = 0.5

class GraphHopperMatcher @Inject constructor(
    private val graphHopper: GraphHopperAPI
) : Matcher {
    override fun match(wayPassenger: Way, wayDriver: Way): Lift? {
        return Scout(graphHopper, wayPassenger, wayDriver).match()
    }
}

private class Scout(
    private val graphHopper: GraphHopperAPI,
    private val wayPassenger: Way,
    private val wayDriver: Way
) {
    private val maxDetourTime = wayDriver.max_detour_time ?: Way.DEFAULT_DRIVER_MAX_DETOUR_TIME
    private val wayPointsPassenger = wayPassenger.waypoints.map { it.toGHPoint() }
    private val wayPointsDriver = wayDriver.waypoints.map { it.toGHPoint() }
    private val pathDirect = getPath(wayPointsDriver[0], wayPointsDriver[1])  // FIXME cache in WayContext
    private val timeDirect = pathDirect.time
    private val distanceDirect = pathDirect.distance
    private val pathToPickup = getPath(wayPointsDriver[0], wayPointsPassenger[0])
    private val passengerPath = getPath(wayPointsPassenger[0], wayPointsPassenger[1])

    fun match(): Lift? {
        return total_match() ?: sectional_match()
    }

    fun total_match(): Lift? {
        val pathFromDropOff = getPath(wayPointsPassenger[1], wayPointsDriver[1])
        val totalTime = pathToPickup.time + passengerPath.time + pathFromDropOff.time
        val detourTime = totalTime - timeDirect
        val totalDistance = pathToPickup.distance + passengerPath.distance + pathFromDropOff.distance
        val detourDistance = totalDistance - distanceDirect
        if (detourTime > maxDetourTime) {
            return null
        }
        if (detourDistance > passengerPath.distance) {
            // Don't suggest lifts that would cause more traffic than both partys going with separate cars
            // This also rules out nonsensical lifts, as for example bringing someone into the
            // opposite direction before going the normal path
            return null
        }

        val rating = passengerPath.distance / detourDistance
        return createLift(wayPassenger.waypoints.last(), passengerPath, detourDistance, detourTime, false, rating)
    }

    fun sectional_match(): Lift? {
        if (wayPassenger.sectional_match != true) {
            return null
        }
        val pathFromPickupToDriverDestination = getPath(wayPointsPassenger[0], wayPointsDriver[1])
        val timeWithPickup = pathToPickup.time + pathFromPickupToDriverDestination.time
        val distanceWithPickup = pathToPickup.distance + pathFromPickupToDriverDestination.distance
        val detourTime = timeWithPickup - timeDirect
        val detourDistance = distanceWithPickup - distanceDirect
        if (detourTime > maxDetourTime) {
            return null
        }
        val sharedPath = sharedPath() ?: return null
        val sharedDistance = sharedPath.distance
        if (sharedDistance == 0.0 || detourDistance / sharedDistance > MAX_DETOUR_DISTANCE_RATIO) {
            return null
        }
        // The rating for sectional matches is always negative, because total matches should be favored
        // The ore detour we have in proportion to the shared distance, the worse the rating gets
        val rating = -detourDistance / sharedDistance
        val dropOffPoint = sharedPath.points.last().toOHPoint()
        return createLift(dropOffPoint, sharedPath, detourDistance, detourTime, true, rating)
    }

    private fun createLift(
        dropOffPoint: OHPoint,
        sharedPath: ResponsePath,
        detourDistance: Double,
        detourTime: Long,
        isSectional: Boolean,
        rating: Double
    ): Lift {
        val sharedDistance = sharedPath.distance
        val timeToPickup = pathToPickup.time
        val pickupTime = wayDriver.start_time + timeToPickup
        val dropOffTime = pickupTime + sharedPath.time
        val priceModel = PriceModel.selectPriceModel()
        val price = priceModel.getPrice(sharedDistance, detourDistance)
        return Lift(
            id = UUID.randomUUID().toString(),
            status = Lift.Status.SUGGESTED,
            passenger_way_id = wayPassenger.id,
            driver_way_id = wayDriver.id,
            pick_up_point = wayPassenger.waypoints[0],
            pick_up_hint = BigDecimal(3),
            drop_off_point = dropOffPoint,
            drop_off_hint = BigDecimal(6),
            rating = rating,
            shared_distance = sharedDistance,
            pickup_time = pickupTime,
            drop_off_time = dropOffTime,
            price = price,
            currency = priceModel.currency.currencyCode,
            detour_distance = detourDistance,
            detour_time = detourTime,
            sectional = isSectional
        )
    }

    //  Actually, the shared Path is already calculated. It consists of the first matchLength points in passengerPoints/driverPoints
    // Unfortunately, the graphhopper-api only delivers the points, not the time or distance, so we have to calculate the route again
    private fun sharedPath(): ResponsePath? {
        val driverPathAfterPickup = getPath(Arrays.asList(wayPointsPassenger[0], wayPointsDriver[1]), true)
        val passengerPath = getPath(Arrays.asList(wayPointsPassenger[0], wayPointsDriver[1]), true)
        val driverPoints = driverPathAfterPickup.points
        val passengerPoints = passengerPath.points
        val matchLength = getMatchLength(driverPoints, passengerPoints)
        return if (matchLength <= 0) {
            null
        } else getPath(passengerPoints[0], passengerPoints[matchLength])
    }

    private fun getMatchLength(p: PointList, q: PointList): Int {
        val nPoints = q.size
        var i = 0
        while (i < nPoints && q[i] == p[i]) {
            i++
        }
        if (i == 0) {
            logger.error("Something's wrong here. The two ways should start at the same point.")
        }
        return i - 1
    }

    private fun getPath(vararg ghPoints: GHPoint): ResponsePath {
        return getPath(Arrays.asList(*ghPoints), false)
    }

    private fun getPath(ghPoints: List<GHPoint>, calcPoints: Boolean): ResponsePath {
        val requestPoints: List<GHPoint> = ArrayList(ghPoints)
        val request = GHRequest(requestPoints)
        // the profile must match the one we set up in the config
        request.profile = "car"
        request.hints.putObject(Parameters.Routing.CALC_POINTS, calcPoints)
        val response = graphHopper.route(request)
        return response.best
    }
}