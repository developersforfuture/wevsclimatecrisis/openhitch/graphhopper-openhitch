package de.wifaz.oh.server;

import com.graphhopper.GraphHopper;
import com.graphhopper.GraphHopperConfig;
import com.graphhopper.http.GraphHopperBundleConfiguration;
import de.wifaz.oh.server.photon.HitchPhotonResource;
import io.dropwizard.ConfiguredBundle;
import io.dropwizard.setup.Environment;
import org.glassfish.hk2.api.Factory;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import javax.inject.Inject;
import javax.ws.rs.client.ClientBuilder;

public class OpenHitchBundle implements ConfiguredBundle<GraphHopperBundleConfiguration> {

    @Override
    public void run(GraphHopperBundleConfiguration configuration, Environment environment) {
        environment.jersey().register(new AbstractBinder() {
            @Override
            protected void configure() {
                this.bindFactory(HitchDBFactory.class).to(HitchDB.class);
                this.bindFactory(MatcherFactory.class).to(Matcher.class);
                this.bindFactory(MessengerFactory.class).to(Messenger.class);
                this.bindFactory(HitchPhotonResourceFactory.class).to(HitchPhotonResource.class);
                this.bindFactory(DebuggingResourceFactory.class).to(DebuggingResource.class);
                this.bindFactory(HitchDBHookFactory.class).to(HitchDBHook.class);
            }
        });
        environment.jersey().register(AuthenticationFilter.class);
        environment.jersey().register(HitchResource.class);
        environment.jersey().register(HitchPhotonResource.class);
        environment.jersey().register(DebuggingResource.class);
    }

    static class HitchDBFactory implements Factory<HitchDB> {
        // FIXME what is the right way to make this a singleton?
        static HitchDB ramHitchDB = null;
        @Inject
        GraphHopperConfig config;
        @Inject
        HitchDBHook hook;

        @Override
        public HitchDB provide() {
            String dbType = config.getString("openhitch.db.type", "ram");

            HitchDB db = null;
            if ("ram".equals(dbType)) {
                if (ramHitchDB == null) {
                    ramHitchDB = new RamHitchDB(hook);
                }
                db = ramHitchDB;
            } else if ("mongo".equals(dbType)) {
                String dbName = config.getString("openhitch.db.name", "ohtest");
                db = new MongoHitchDB(dbName, hook);
            } else {
                throw new RuntimeException("Invalid value for openhitch.db.type: " + dbType);
            }

            return db;
        }

        @Override
        public void dispose(HitchDB db) {
            if (db instanceof MongoHitchDB) {
                ((MongoHitchDB) db).close();
            }
        }
    }

    static class MatcherFactory implements Factory<Matcher> {

        @Inject
        GraphHopper graphHopper;

        @Override
        public Matcher provide() {
            return new GraphHopperMatcher(graphHopper);
        }

        @Override
        public void dispose(Matcher matcher) {

        }
    }

    static class MessengerFactory implements Factory<Messenger> {
        static Messenger messenger = null;

        @Inject
        GraphHopperConfig config;


        @Override
        public Messenger provide() {
            String mqttServer = config.getString("openhitch.mqtt.server", "");
            String mqttUser = config.getString("openhitch.mqtt.user", "");
            String mqttPassword = config.getString("openhitch.mqtt.password", "");
            if (messenger == null) {
                messenger = new MqttMessenger(mqttServer, mqttUser, mqttPassword);
            }
            return messenger;
        }

        @Override
        public void dispose(Messenger messenger) {

        }
    }

    static class HitchDBHookFactory implements Factory<HitchDBHook> {
        @Inject
        Messenger messenger;

        @Override
        public HitchDBHook provide() {
            return new HitchDBHook(messenger);
        }

        @Override
        public void dispose(HitchDBHook messenger) {

        }

    }

    static class HitchPhotonResourceFactory implements Factory<HitchPhotonResource> {

        @Inject
        GraphHopperConfig config;

        @Override
        public HitchPhotonResource provide() {
            return new HitchPhotonResource(
                    config.getString("photon.url", "https://photon.komoot.io/api/"),
                    config.getString("photon.reverse_url", "https://photon.komoot.io/reverse"),
                    ClientBuilder.newClient());
        }

        @Override
        public void dispose(HitchPhotonResource resource) {
            resource.dispose();
        }
    }

    static class DebuggingResourceFactory implements Factory<DebuggingResource> {
        @Inject
        GraphHopperConfig config;

        @Inject
        HitchDB db;

        @Override
        public DebuggingResource provide() {
            boolean debug = config.getBool("openhitch.debug", false);
            if (debug) {
                return new DebuggingResource(db);
            } else {
                return null;
            }
        }

        @Override
        public void dispose(DebuggingResource resource) {
        }
    }

}
