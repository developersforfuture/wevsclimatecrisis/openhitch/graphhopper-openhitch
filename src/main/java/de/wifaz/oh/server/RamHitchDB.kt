package de.wifaz.oh.server

import de.wifaz.oh.protocol.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set


/**
 * Central storage for hitch data, in particular the published ways
 *
 * This implementation is for testing purposes only. All information resides in RAM and is lost, when the server is shut down.
 * @see MongoHitchDB for an implementation providing persistency
 */
class RamHitchDB(
    private val hook: HitchDBHook
) : HitchDB {

    private val users: DBMap<User> = DBMap()
    private val registrationStates: DBMap<RegistrationState> = DBMap()
    private val tokenUser: MutableMap<String, String> = HashMap()
    private val userTokens: DBMap<MutableSet<String>> = DBMap()
    private val feedbacks: DBMap<Feedback> = DBMap()
    private val userWays: DBMap<MutableSet<String>> = DBMap()
    private val lifts: DBMap<Lift> = DBMap()
    private val wayContexts: DBMap<WayContext> = DBMap()
    private val profilePictures: DBMap<ByteArray> = DBMap()


    override fun <T>transaction(code: () -> T) : T {
        synchronized(this) {
            return code()
        }
    }

    override fun putUser(user: User) {
        if (!hasUser(user.id)) {
            userTokens[user.id] = HashSet<String>()
            userWays[user.id] = HashSet<String>()
        }
        users[user.id] = user
    }

    override fun hasUser(userId: String): Boolean {
        return users.containsKey(userId)
    }

    override fun removeUser(userId: String) {
        userWays[userId].forEach { removeWay(it) }
        userWays.remove(userId)
        userTokens[userId].forEach { tokenUser.remove(it) }
        userTokens.remove(userId)
        users.remove(userId)
        profilePictures.remove(userId)
    }

    override fun getPersonas(): List<User> {
        return users.values.filter { it.id.startsWith("idPersona") }
    }

    override fun getProfilePicture(userId: String): ByteArray? {
        return profilePictures[userId]
    }

    override fun putProfilePicture(userId: String, profilePicture: ByteArray) {
        profilePictures[userId] = profilePicture
    }

    override fun hasProfilePicture(userId: String): Boolean {
        return profilePictures.containsKey(userId)
    }

    override fun getFeedback(feedbackId: String): Feedback {
        return feedbacks.get(feedbackId)
    }

    override fun putFeedback(feedback: Feedback) {
        feedbacks[feedback.id] = feedback
    }

    override fun putToken(userId: String, token: String) {
        userTokens[userId].add(token)
        tokenUser[token] = userId
    }

    override fun hasToken(userId: String, token: String): Boolean {
        return userTokens[userId].contains(token)
    }

    override fun getTokenUser(token: String): String? {
        return tokenUser[token]
    }

    override fun removeToken(userId: String, token: String) {
        userTokens[userId].remove(token)
        tokenUser.remove(token)
    }

    override fun putRegistrationState(registrationState: RegistrationState) {
        registrationStates.put(registrationState.user_id, registrationState)
    }

    override fun getRegistrationState(userId: String): RegistrationState {
        return registrationStates[userId]
    }

    override fun getLift(liftId: String): Lift {
        return lifts[liftId]
    }

    override fun hasLift(liftId: String): Boolean {
        return lifts.containsKey(liftId)
    }

    override fun putLift(lift: Lift) {
        lifts[lift.id] = lift
        wayContexts[lift.driver_way_id].lifts[lift.id] = lift
        wayContexts[lift.passenger_way_id].lifts[lift.id] = lift
    }

    override fun removeLift(lift: Lift) {
        hook.onRemoveLift(this, lift)
        val id = lift.id
        lifts.remove(id)
        wayContexts[lift.driver_way_id].lifts.remove(id)
        wayContexts[lift.passenger_way_id].lifts.remove(id)
    }

    override fun putWay(way: Way) {
        if (hasWay(way.id)) {
            val wayContext = wayContexts[way.id]
            wayContext.way = way
        } else {
            wayContexts[way.id] = WayContext(way)
            userWays[way.user_id].add(way.id)
        }
    }

    override fun hasWay(wayId: String): Boolean {
        return wayContexts.containsKey(wayId)
    }

    override fun removeWay(wayId: String) {
        val wayContext = wayContexts[wayId]
        for (lift in ArrayList(wayContext.lifts.values)) {
            removeLift(lift)
        }
        val way = wayContexts[wayId].way
        userWays[way.user_id].remove(wayId)
        wayContexts.remove(wayId)
    }

    override fun getAllWays(): ArrayList<Way> {
        return ArrayList(wayContexts.values.map { it.way })
    }

    override fun getUser(userId: String): User {
        return users[userId]
    }

    override fun getWay(wayId: String): Way {
        return wayContexts[wayId].way
    }

    override fun isClean(): Boolean {
        // for now, ignore listeners
        return users.isEmpty()
                && userWays.isEmpty()
                && userTokens.isEmpty()
                && tokenUser.isEmpty()
                && lifts.isEmpty()
                && wayContexts.isEmpty()
                && feedbacks.isEmpty()
    }

    override fun getWaysForUser(userId: String?): List<Way> {
        return userWays[userId]!!.map { getWay(it) }  // FIXME: warum ist hier null möglich, sollte doch eigentlich von DBMap schon gehandlet werden
    }

    override fun dump(): DBDump {
        return DBDump(
            time = Date().time,
            users = ArrayList(users.values),
            user_tokens = ArrayList(getAllUserTokens()),
            registration_states = ArrayList(registrationStates.values),
            ways = getAllWays(),
            lifts = ArrayList(lifts.values),
            feedbacks = ArrayList(feedbacks.values),
            profile_pictures = profilePictures.mapValues { Base64.getEncoder().encodeToString(it.value) }
        )
    }

    private fun getAllUserTokens(): ArrayList<UserToken> {
        val result = ArrayList<UserToken>()
        for (entry in userTokens) {
            result.addAll(entry.value.map { UserToken(entry.key, it) })
        }
        return result
    }

    override fun restore(dbDump: DBDump) {
        clear()
        dbDump.users.forEach { putUser(it) }
        dbDump.user_tokens.forEach { putToken(it.userId, it.token) }
        dbDump.registration_states.forEach { putRegistrationState(it) }
        dbDump.ways.forEach { putWay(it) }
        dbDump.lifts.forEach { putLift(it) }
        dbDump.feedbacks.forEach { putFeedback(it) }
        dbDump.profile_pictures.forEach { putProfilePicture(it.key, Base64.getDecoder().decode(it.value)) }
    }

    private fun clear() {
        lifts.clear()
        wayContexts.clear()
        userWays.clear()
        userTokens.clear()
        tokenUser.clear()
        registrationStates.clear()
        users.clear()
        profilePictures.clear()
        feedbacks.clear()
    }

    override fun getLifts(wayId: String): List<Lift> {
        val wayContext = wayContexts[wayId]
        return ArrayList(wayContext.lifts.values)
    }

    override fun getSuggestedLiftsOrderedByRating(wayId: String): List<Lift> {
        return getLifts(wayId)
            .filter { lift -> lift.status == Lift.Status.SUGGESTED }
            .sortedBy { lift -> -lift.rating }
    }

    override fun isEngaged(wayId: String): Boolean {
        val lifts = getLifts(wayId)
        val lift = lifts.firstOrNull { it.status in Lift.engagedStates }
        return lift != null

    }

}

private class WayContext(
    var way: Way
) {
    val lifts: DBMap<Lift> = DBMap()
}

class DBMap<T> : HashMap<String, T>() {
    override fun get(key: String): T {
        return super.get(key) ?: throw HitchDBException("Reference to unknown key $key")
    }
}