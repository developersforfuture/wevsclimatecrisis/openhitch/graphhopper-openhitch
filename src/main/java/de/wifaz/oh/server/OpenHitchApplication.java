package de.wifaz.oh.server;

import com.graphhopper.http.GraphHopperApplication;
import com.graphhopper.http.GraphHopperServerConfiguration;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class OpenHitchApplication extends Application<GraphHopperServerConfiguration> {

    private final GraphHopperApplication ghApplication = new GraphHopperApplication();

    public static void main(String[] args) throws Exception {
        new OpenHitchApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<GraphHopperServerConfiguration> bootstrap) {
        bootstrap.addBundle(new OpenHitchBundle());
        ghApplication.initialize(bootstrap);
    }

    @Override
    public void run(GraphHopperServerConfiguration configuration, Environment environment) {
        ghApplication.run(configuration, environment);
    }

}
