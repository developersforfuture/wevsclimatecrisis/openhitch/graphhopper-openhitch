package de.wifaz.oh.server

import com.graphhopper.util.shapes.GHPoint
import de.wifaz.oh.protocol.OHPoint

fun OHPoint.toGHPoint(): GHPoint {
    return GHPoint(latitude, longitude)
}

fun GHPoint.toOHPoint(): OHPoint {
    return OHPoint.fromLngLat(getLon(), getLat())
}
