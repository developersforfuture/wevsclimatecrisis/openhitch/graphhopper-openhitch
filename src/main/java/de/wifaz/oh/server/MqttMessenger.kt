package de.wifaz.oh.server

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import de.wifaz.oh.protocol.HitchMessage
import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import org.eclipse.paho.client.mqttv3.MqttException
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import org.slf4j.LoggerFactory
import javax.inject.Singleton

@Singleton
class MqttMessenger(private val mqttServerUrl: String, private val mqttUser: String, private val mqttPassword: String) :
    Messenger {
    private val persistence = MemoryPersistence()
    val mqttClient: MqttClient

    override fun close() {
        try {
            mqttClient.disconnect()
            println("Disconnected")
        } catch (e: MqttException) {
            logger.error("Connection to mqtt-server failed: $e")
        }
    }

    override fun send(user_id: String, payload: HitchMessage?) {
        val json: String
        json = try {
            val mapper = ObjectMapper()
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
            mapper.writeValueAsString(payload)
        } catch (e: JsonProcessingException) {
            e.printStackTrace()
            return
        }
        try {
            logger.debug("Publishing message: $json")
            val message = MqttMessage(json.toByteArray())
            message.qos = qos
            mqttClient.publish(user_id, message)
            println("Message published to topic $user_id: $message")
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(MqttMessenger::class.java)

        // FIXME should be in settings file of some kind
        private const val qos = 1
        private const val clientId = "OpenHitch"
    }

    init {
        mqttClient = MqttClient(mqttServerUrl, clientId, persistence)
        try {
            val connOpts = MqttConnectOptions()
            connOpts.isCleanSession = true
            connOpts.userName = mqttUser
            connOpts.password = mqttPassword.toCharArray()
            println("Connecting to broker: $mqttServerUrl")
            mqttClient.connect(connOpts)
            println("Connected")
        } catch (e: MqttException) {
            // FIXME this is to weak, data is lost. Better shut down or inform clients somehow.
            logger.error("Connection to mqtt-server failed: $e")
        }
    }
}