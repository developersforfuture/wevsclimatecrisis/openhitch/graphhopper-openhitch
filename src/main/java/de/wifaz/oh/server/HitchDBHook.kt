package de.wifaz.oh.server

import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.LiftRemoveMessage
import org.slf4j.LoggerFactory

class HitchDBHook(private val messenger: Messenger) {
    fun onRemoveLift(db: HitchDB, lift: Lift) {
        when (lift.status) {
            Lift.Status.FINISHED, Lift.Status.DRIVER_CANCELED -> {
                val message = LiftRemoveMessage(reference_way_id = lift.passenger_way_id, lift_id = lift.id)
                messenger.send(db.getWay(lift.passenger_way_id).user_id, message)
            }
            Lift.Status.PASSENGER_CANCELED -> {
                val message = LiftRemoveMessage(reference_way_id = lift.driver_way_id, lift_id = lift.id)
                messenger.send(db.getWay(lift.driver_way_id).user_id, message)
            }
            Lift.Status.PREVIEW -> {
                // ignore
            }
            else -> {
                logger.error("Lift removed in unexpected status " + lift.status.name)
            }
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(HitchDBHook::class.java)
    }
}