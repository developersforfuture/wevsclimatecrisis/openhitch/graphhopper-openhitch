package de.wifaz.oh.server

/**
 * Force when-Statement to be exhaustive by appending `.exhaustive` to closing brace
 * See https://proandroiddev.com/til-when-is-when-exhaustive-31d69f630a8b
 */
val <T> T.exhaustive: T
    get() = this