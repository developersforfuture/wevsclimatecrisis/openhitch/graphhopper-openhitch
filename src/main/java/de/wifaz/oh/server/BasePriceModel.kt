package de.wifaz.oh.server

import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import kotlin.math.max

class BasePriceModel(
    override val currency: Currency,
    private val minimumPrice: Double,
    private val pricePerSharedKm: Double,
    private val pricePerDetourKm: Double
) : PriceModel {

    override fun getPrice(shared_distance: Double, detour_distance: Double): BigDecimal {
        var price: Double = pricePerSharedKm * shared_distance / 1000 + pricePerDetourKm * detour_distance / 1000
        price = max(price, minimumPrice)
        return BigDecimal(price).setScale(currency.defaultFractionDigits, RoundingMode.HALF_UP)
    }

}