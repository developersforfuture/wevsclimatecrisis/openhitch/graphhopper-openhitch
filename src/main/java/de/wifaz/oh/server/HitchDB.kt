package de.wifaz.oh.server

import com.fasterxml.jackson.annotation.JacksonInject
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import de.wifaz.oh.protocol.*
import java.util.*
import javax.inject.Singleton
import kotlin.collections.ArrayList

class HitchDBException(message: String) : Exception(message)

@Singleton
interface HitchDB {
    fun <T>transaction(code:()->T) : T

    fun getUser(userId: String): User
    fun putUser(user: User)
    fun hasUser(userId: String): Boolean
    fun removeUser(userId: String)
    fun getPersonas(): List<User>

    fun getProfilePicture(userId: String): ByteArray?
    fun putProfilePicture(userId: String, profilePicture: ByteArray)
    fun hasProfilePicture(userId: String): Boolean

    fun getFeedback(feedbackId: String): Feedback
    fun putFeedback(feedback: Feedback)

    fun putToken(userId: String, token: String)
    fun hasToken(userId: String, token: String): Boolean
    fun getTokenUser(token: String): String?
    fun removeToken(userId: String, token: String)

    fun putRegistrationState(registrationState: RegistrationState)
    fun getRegistrationState(userId: String): RegistrationState

    fun getLift(liftId: String): Lift
    fun hasLift(liftId: String): Boolean
    fun putLift(lift: Lift)
    fun removeLift(lift: Lift)
    fun getLifts(wayId: String): List<Lift>
    fun getSuggestedLiftsOrderedByRating(wayId: String): List<Lift>

    fun getWay(wayId: String): Way
    fun putWay(way: Way)
    fun hasWay(wayId: String): Boolean
    fun removeWay(wayId: String)
    fun getAllWays(): List<Way>
    fun isEngaged(wayId: String): Boolean
    fun getWaysForUser(userId: String?): List<Way>


    //////// Debugging stuff
    /**
     * For testing only. Return true if the database is empty and there is no memory garbage
     */
    fun isClean(): Boolean

    /**
     * For testing and debugging only. Return complete content of the database
     */
    fun dump(): DBDump

    /**
     * For testing and debugging only. Wipe out all databass content and inputs a
     * previously taken database dump
     */
    fun restore(dbDump: DBDump)
}

data class UserToken @JsonCreator constructor(
    @JsonProperty("userId") val userId: String,
    @JsonProperty("token") val token: String
) : java.io.Serializable

data class DBDump @JsonCreator constructor(
    @JsonProperty("time") val time: Long,
    @JsonProperty("users") val users: ArrayList<User>,
    @JsonProperty("user_tokens") val user_tokens: ArrayList<UserToken>,
    @JsonProperty("registration_states") val registration_states: ArrayList<RegistrationState>,
    @JsonProperty("ways") val ways: ArrayList<Way>,
    @JsonProperty("lifts") val lifts: ArrayList<Lift>,
    @JsonProperty("feedbacks") @JacksonInject("feedbacks") var feedbacks: ArrayList<Feedback> = ArrayList(),
    @JsonProperty("profile_pictures") val profile_pictures: Map<String, String>
) : java.io.Serializable