package de.wifaz.oh.server

import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.Way

class TimeMatcher {
    /**
     * @return true, if time windows of wayDriver and wayPassenger intersect
     */
    fun preMatch(wayPassenger: Way, wayDriver: Way): Boolean {
        return wayDriver.start_time <= wayPassenger.end_time && wayDriver.end_time >= wayPassenger.start_time
    }

    fun postMatch(lift: Lift, wayPassenger: Way, wayDriver: Way): Boolean {
        // We are only testing if the passenger is transported within their time window.
        // For the driver, the matcher tests if max_detour_time is adhered to
        return wayPassenger.start_time <= lift.pickup_time && wayPassenger.end_time >= lift.drop_off_time
    }
}
