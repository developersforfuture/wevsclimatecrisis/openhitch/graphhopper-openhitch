package de.wifaz.oh.server

import org.slf4j.LoggerFactory
import java.net.HttpURLConnection
import java.security.Principal
import javax.annotation.Priority
import javax.inject.Inject
import javax.ws.rs.Priorities
import javax.ws.rs.WebApplicationException
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerRequestFilter
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.Response
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.ext.Provider

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
class AuthenticationFilter @Inject constructor(private val db: HitchDB) : ContainerRequestFilter {

    override fun filter(requestContext: ContainerRequestContext) {

        // Get the Authorization header from the request
        val authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION)

        // Validate the Authorization header
        if (!isTokenBasedAuthentication(authorizationHeader)) {
            return abortWithUnauthorized(requestContext)
        }

        // Extract the token from the Authorization header
        val token = authorizationHeader.substringAfter(AUTHENTICATION_SCHEME).trim()
        try {
            val userId = validateToken(token)
            replaceSecurityContext(requestContext, userId)
        } catch (e: Exception) {
            abortWithUnauthorized(requestContext)
        }
    }

    private fun replaceSecurityContext(requestContext: ContainerRequestContext, user_id: String) {
        val currentSecurityContext = requestContext.securityContext
        requestContext.securityContext = object : SecurityContext by currentSecurityContext {
            override fun getUserPrincipal(): Principal {
                return Principal { user_id }
            }

            override fun isUserInRole(role: String): Boolean {
                return true
            }

            override fun getAuthenticationScheme(): String {
                return AUTHENTICATION_SCHEME
            }
        }
    }

    private fun isTokenBasedAuthentication(authorizationHeader: String?): Boolean {
        // Check if the Authorization header is valid
        // It must not be null and must be prefixed with "Bearer" plus a whitespace
        // The authentication scheme comparison must be case-insensitive
        return authorizationHeader?.toLowerCase()?.startsWith("${AUTHENTICATION_SCHEME.toLowerCase()} ") ?: false
    }

    private fun abortWithUnauthorized(requestContext: ContainerRequestContext) {
        // Abort the filter chain with a 401 status code response
        // The WWW-Authenticate header is sent along with the response
        requestContext.abortWith(
            Response.status(Response.Status.UNAUTHORIZED)
                .header(HttpHeaders.WWW_AUTHENTICATE, "$AUTHENTICATION_SCHEME realm=\"$REALM\"")
                .build()
        )
    }

    /**
     * Check if the token is valid and returns the user'S id the token belongs to. Throws an
     * exception if the token is invalid.
     *
     * @param token the token to check
     * @return the ID of the user this token belongs to
     * @throws WebApplicationException if the token is not a valid token
     */
    private fun validateToken(token: String): String {
        // Validate the token using the hashed form that is stored in the DB
        val userId = db.getTokenUser(TokenHelper.asDbHash(token))
        if (userId == null) {
            LOGGER.info("Attempt of unauthorized access")
            throw WebApplicationException(HttpURLConnection.HTTP_UNAUTHORIZED)
        }
        return userId
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(AuthenticationFilter::class.java)

        private const val REALM = "openhitch"
        private const val AUTHENTICATION_SCHEME = "Bearer"
    }
}