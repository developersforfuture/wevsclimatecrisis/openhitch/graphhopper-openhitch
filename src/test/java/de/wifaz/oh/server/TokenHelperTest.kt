package de.wifaz.oh.server

import org.junit.jupiter.api.Test
import java.util.*
import kotlin.test.assertEquals

internal class TokenHelperTest {

    @Test
    fun asDbHash_Base64EncodedString() {
        val userIdBase64 = toBase64("idPersona_Sabine")
        val userIdDbHash = "V5wXHO48L1r6HFv0OoJXytU3UIMMWobh8WqULinlY/I="

        assertEquals(userIdDbHash, TokenHelper.asDbHash(userIdBase64))
    }

    @Test
    fun asDbHash_ByteArray() {
        val userIdBytes = "idPersona_Sabine".encodeToByteArray()
        val userIdDbHash = "V5wXHO48L1r6HFv0OoJXytU3UIMMWobh8WqULinlY/I="
        assertEquals(userIdDbHash, TokenHelper.asDbHash(userIdBytes))
    }

    @Test
    fun newRandomToken() {
        val token = TokenHelper.newRandomToken()
        val tokenBytes = fromBase64(token)
        assertEquals(tokenBytes.size * Byte.SIZE_BITS, 256)
        assertEquals(toBase64(tokenBytes), token)
    }

    private fun toBase64(s: String): String {
        return toBase64(s.encodeToByteArray())
    }

    private fun toBase64(bytes: ByteArray): String {
        return Base64.getEncoder().encodeToString(bytes)
    }

    private fun fromBase64(base64String: String): ByteArray {
        return Base64.getDecoder().decode(base64String)
    }

}