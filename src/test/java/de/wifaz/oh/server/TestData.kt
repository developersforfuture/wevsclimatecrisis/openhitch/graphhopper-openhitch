package de.wifaz.oh.server

import de.wifaz.oh.protocol.*
import java.math.BigDecimal
import java.util.*

open class TestData {

    companion object {
        val SECOND = 1000
        val MINUTE = SECOND * 60
        val HOUR = MINUTE * 60
        val DAY = HOUR * 24
        val TIME1 = Date().time + DAY  // tomorrow
        val TIME2 = TIME1 + MINUTE // one minute later
        val DRIVER = User(
            id = "user_idDriver",
            first_name = "Ferdinand",
            last_name = "Fahrer",
            nick_name = "Ferdinand",
            phone = null
        )
        val PASSENGER = User(
            id = "user_idPassenger",
            first_name = "Moritz",
            last_name = "Mitfahrer",
            nick_name = "Moritz",
            phone = null
        )

        // some points in Berlin
        val P_HAUSBURGSTR = OHPoint.fromLngLat(13.45328, 52.52617)
        val P_TORSTR = OHPoint.fromLngLat(13.3968, 52.5287)
        val P_COTHENIUSSTR = OHPoint.fromLngLat(13.4480129, 52.5282619)
        val P_ZEHDENICKERSTR = OHPoint.fromLngLat(13.4058858, 52.5299841)

        // some driver way in Berlin
        val WAY_DRIVER = Way(
            id = "idWayDriver",
            status = Way.Status.PUBLISHED,
            user_id = DRIVER.id,
            role = Role.DRIVER,
            waypoints = arrayListOf(P_HAUSBURGSTR, P_TORSTR),
            start_time = TIME1,
            end_time = TIME1 + 2 * HOUR,
            seats = 1,
            autocommit = false,
            max_detour_time = 5 * 60 * 1000
        )

        // some rider way in Berlin that matches with WAY_DRIVER. The waypoints are not directly on the route of WAY_DRIVER, but close
        val WAY_PASSENGER = Way(
            id = "idWayPassenger",
            status = Way.Status.PUBLISHED,
            user_id = PASSENGER.id,
            role = Role.PASSENGER,
            waypoints = arrayListOf(P_COTHENIUSSTR, P_ZEHDENICKERSTR),
            start_time = TIME2,
            end_time = TIME1 + 2 * HOUR,
            seats = 1,
            autocommit = false,
            max_detour_time = null
        )

        // same as WAY_DRIVER, but with a different id and one day later
        val WAY2_DRIVER =
            WAY_DRIVER.copy(id = "idWay2Driver", start_time = TIME1 + DAY, end_time = TIME1 + DAY + 2 * HOUR)

        // Lift resulting from matching WAY_DRIVER, WAY_PASSENGER. Values are only given approximately.
        // This is not intended for comparison with real geo-information
        val LIFT1 = Lift(
            id = "idLIFT1",
            status = Lift.Status.SUGGESTED,
            passenger_way_id = WAY_PASSENGER.id,
            driver_way_id = WAY_DRIVER.id,
            pick_up_point = P_COTHENIUSSTR,
            pick_up_hint = BigDecimal(3),
            drop_off_point = P_ZEHDENICKERSTR,
            drop_off_hint = BigDecimal(6),
            rating = 3.4,
            shared_distance = 3.4,
            pickup_time = TIME1 + 5 * 60 * 1000,  // 5 minutes after TIME1
            drop_off_time = TIME1 + 20 * 60 * 1000,  // 20 minutes after TIME1
            price = BigDecimal(1),
            currency = "EUR",
            detour_distance = 0.2,
            detour_time = 1000,
            sectional = false
        )
    }
}