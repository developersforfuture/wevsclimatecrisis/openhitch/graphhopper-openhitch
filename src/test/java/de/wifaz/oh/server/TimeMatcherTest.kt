package de.wifaz.oh.server

import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertFalse

class TimeMatcherTest : TestData() {
    var timeMatcher = TimeMatcher()

    @Test
    fun preMatchIntersectingTimeWindows() {
        var matches: Boolean = timeMatcher.preMatch(WAY_PASSENGER, WAY_DRIVER)
        assertTrue(matches)
    }

    @Test
    fun dontPreMatchDisjointTimeWindows() {
        var matches: Boolean = timeMatcher.preMatch(WAY_PASSENGER, WAY2_DRIVER)
        assertFalse(matches)
    }

    @Test
    fun postMatchLiftInBothTimeWindows() {
        var matches: Boolean = timeMatcher.postMatch(LIFT1, WAY_PASSENGER, WAY_DRIVER)
        assertTrue(matches)
    }

    @Test
    fun dontPostMatchDropoffAfterPassengerEndTime() {
        var matches: Boolean = timeMatcher.postMatch(
            LIFT1.copy(drop_off_time = WAY_PASSENGER.end_time + MINUTE),
            WAY_PASSENGER,
            WAY_DRIVER
        )
        assertFalse(matches)
    }

    @Test
    fun dontPostMatchPickupBeforePassengerStartTime() {
        var matches: Boolean = timeMatcher.postMatch(
            LIFT1.copy(pickup_time = WAY_PASSENGER.start_time - MINUTE),
            WAY_PASSENGER,
            WAY_DRIVER
        )
        assertFalse(matches)
    }

}