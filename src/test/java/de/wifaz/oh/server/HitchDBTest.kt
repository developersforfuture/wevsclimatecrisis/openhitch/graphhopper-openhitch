package de.wifaz.oh.server

import com.nhaarman.mockitokotlin2.mock
import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.Way
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

internal class MongoHitchDBTest : AbstractHitchDBTest() {
    override fun getDb(): MongoHitchDB {
        val db = MongoHitchDB("oh_unittest", mock())
        db.database.drop()
        return db
    }
}

internal class RamHitchDBTest : AbstractHitchDBTest() {
    override fun getDb(): HitchDB {
        return RamHitchDB(mock())
    }
}

internal abstract class AbstractHitchDBTest : TestData() {
    abstract fun getDb(): HitchDB

    @Test
    fun crudUser() {
        var db = getDb()
        assertFalse(db.hasUser(DRIVER.id))
        db.putUser(DRIVER)
        assertTrue(db.hasUser(DRIVER.id))
        val driver = db.getUser(DRIVER.id)
        assertEquals(DRIVER.last_name, driver.last_name)
        val newName = "Dr. " + driver.last_name
        db.putUser(DRIVER.copy(last_name = newName))
        val updatedUser = db.getUser(DRIVER.id)
        assertEquals(newName, updatedUser.last_name)
        db.removeUser(DRIVER.id)
        assertTrue(db.isClean())
    }

    @Test
    fun userTokens() {
        var db = getDb()
        db.putUser(DRIVER)
        var token = "abc123"
        assertFalse(db.hasToken(DRIVER.id, token))
        assertNull(db.getTokenUser(token))
        db.putToken(DRIVER.id, token)
        assertTrue(db.hasToken(DRIVER.id, token))
        assertEquals(DRIVER.id, db.getTokenUser(token))
        var otherToken = "xyz789"
        assertFalse(db.hasToken(DRIVER.id, otherToken))
        db.putToken(DRIVER.id, otherToken)
        assertTrue(db.hasToken(DRIVER.id, otherToken))
        db.removeToken(DRIVER.id, token)
        assertFalse(db.hasToken(DRIVER.id, token))
        db.removeUser(DRIVER.id)

        // Must throw exception when asked for token of a non existent user
        Assertions.assertThrows(HitchDBException::class.java) {
            db.hasToken(DRIVER.id, otherToken)
        }

        // otherToken must vanish with user
        assertTrue(db.isClean())
    }

    @Test
    fun crudWay() {
        var db = getDb()
        assertFalse(db.hasWay(WAY_DRIVER.id))

        db.putUser(DRIVER)
        db.putWay(WAY_DRIVER)
        assertTrue(db.hasWay(WAY_DRIVER.id))
        assertFalse(db.hasWay("UnusedId"))

        var w = db.getWay(WAY_DRIVER.id)
        assertEquals(WAY_DRIVER.id, w.id)
        assertEquals(WAY_DRIVER.start_time, w.start_time)

        var ways = db.getAllWays()
        assertEquals(1, ways.size)
        w = ways[0]
        assertNotNull(w)
        assertEquals(WAY_DRIVER.id, w.id)
        assertEquals(WAY_DRIVER.start_time, w.start_time)

        db.removeWay(WAY_DRIVER.id)
        assertFalse(db.hasWay(WAY_DRIVER.id))

        db.removeUser(DRIVER.id)
        assertTrue(db.isClean())
    }

    @Test
    fun failPutWayForUnknownUser() {
        var db = getDb()
        Assertions.assertThrows(HitchDBException::class.java) {
            db.putWay(WAY_DRIVER)
        }
    }

    @Test
    fun failGetUnknownUser() {
        var db = getDb()
        Assertions.assertThrows(HitchDBException::class.java) {
            db.getUser("foo")
        }
    }

    @Test
    fun failGetUnknownWay() {
        var db = getDb()
        Assertions.assertThrows(HitchDBException::class.java) {
            db.getWay("foo")
        }
    }

    @Test
    fun failGetUnknownLift() {
        var db = getDb()
        Assertions.assertThrows(HitchDBException::class.java) {
            db.getLift("foo")
        }
    }

    @Test
    fun removeWayWithUser() {
        var db = getDb()
        db.putUser(DRIVER)
        db.putWay(WAY_DRIVER)
        assertTrue(db.hasUser(DRIVER.id))
        assertTrue(db.hasWay(WAY_DRIVER.id))
        db.removeUser(DRIVER.id)
        assertFalse(db.hasUser(DRIVER.id))
        assertFalse(db.hasWay(WAY_DRIVER.id))
        assertTrue(db.isClean())
    }

    @Test
    fun crudLift() {
        var db = getDb()
        db.putUser(DRIVER)
        db.putUser(PASSENGER)
        db.putWay(WAY_DRIVER)
        db.putWay(WAY_PASSENGER)
        assertFalse(db.hasLift(LIFT1.id))
        db.putLift(LIFT1)
        var lift = db.getLift(LIFT1.id)
        assertEquals(LIFT1.id, lift.id)
        assertEquals(WAY_DRIVER.id, lift.driver_way_id)
        assertEquals(WAY_PASSENGER.id, lift.passenger_way_id)
        db.removeLift(LIFT1)
        assertFalse(db.hasLift(LIFT1.id))

        db.removeWay(WAY_DRIVER.id)
        db.removeWay(WAY_PASSENGER.id)
        db.removeUser(DRIVER.id)
        db.removeUser(PASSENGER.id)
        assertTrue(db.isClean())
    }

    @Test
    fun wayStatusUpdateRetainsLift() {
        var db = getDb()
        db.putUser(DRIVER)
        db.putUser(PASSENGER)
        db.putWay(WAY_DRIVER)
        assertEquals(WAY_DRIVER, db.getWay(WAY_DRIVER.id))
        db.putWay(WAY_PASSENGER)
        db.putLift(LIFT1)
        val newWay = WAY_DRIVER.copy(status = Way.Status.STARTED)
        db.putWay(newWay)
        assertEquals(newWay, db.getWay(WAY_DRIVER.id))
        assertEquals(LIFT1, db.getLift(LIFT1.id))
        db.removeUser(DRIVER.id)
        db.removeUser(PASSENGER.id)
        assertTrue(db.isClean())
    }

    @Test
    fun autoRemoveLift() {
        var db = getDb()
        db.putUser(DRIVER)
        db.putUser(PASSENGER)
        db.putWay(WAY_DRIVER)
        db.putWay(WAY_PASSENGER)
        db.putLift(LIFT1)
        assertTrue(db.hasLift(LIFT1.id))
        db.removeWay(WAY_DRIVER.id)
        assertFalse(db.hasLift(LIFT1.id))
        db.removeUser(DRIVER.id)
        db.removeUser(PASSENGER.id)
        assertTrue(db.isClean())
    }

    @Test
    fun callListener() {
        var db = getDb()
        val hookMock: HitchDBHook = mock(HitchDBHook::class.java)
        // FIXME set hook
        db.putUser(DRIVER)
        db.putUser(PASSENGER)
        db.putWay(WAY_DRIVER)
        db.putWay(WAY_PASSENGER)
        db.putLift(LIFT1)
        db.removeWay(WAY_DRIVER.id)
        verify(hookMock).onRemoveLift(db, LIFT1)
        db.removeUser(DRIVER.id)
        db.removeUser(PASSENGER.id)
        assertTrue(db.isClean())
    }

    @Test
    fun getSuggestedLiftsOrderedByRating() {
        var db = getDb()

        db.putUser(PASSENGER)
        db.putWay(WAY_PASSENGER)

        fun putLift(i: Int, status: Lift.Status, rating: Double) {
            db.putUser(DRIVER.copy(id = "driverId_$i"))
            db.putWay(WAY_DRIVER.copy(id = "driverWayId_$i", user_id = "driverId_$i"))
            db.putLift(LIFT1.copy(id = "liftId_$i", status = status, driver_way_id = "driverWayId_$i", rating = rating))
        }

        putLift(1, Lift.Status.SUGGESTED, 2.0)
        putLift(2, Lift.Status.SUGGESTED, 5.0)
        putLift(3, Lift.Status.REQUESTED, 4.0)
        putLift(4, Lift.Status.SUGGESTED, 3.0)

        val orderedLifts = db.getSuggestedLiftsOrderedByRating(WAY_PASSENGER.id)
        assertEquals(3, orderedLifts.size)
        assertEquals("liftId_2", orderedLifts[0].id)
        assertEquals("liftId_4", orderedLifts[1].id)
        assertEquals("liftId_1", orderedLifts[2].id)
    }

    @Test
    fun isEngaged() {
        var db = getDb()
        db.putUser(PASSENGER)
        db.putUser(DRIVER)
        db.putWay(WAY_PASSENGER)
        db.putWay(WAY_DRIVER)
        db.putLift(LIFT1)
        assertFalse(db.isEngaged(WAY_PASSENGER.id))
        val lift2 = LIFT1.copy(id = "idLift2", status = Lift.Status.ACCEPTED)
        db.putLift(lift2)
        assertTrue(db.isEngaged(WAY_PASSENGER.id))
        db.removeLift(LIFT1)
        assertTrue(db.isEngaged(WAY_PASSENGER.id))
        db.removeLift(lift2)
        assertFalse(db.isEngaged(WAY_PASSENGER.id))
    }
}