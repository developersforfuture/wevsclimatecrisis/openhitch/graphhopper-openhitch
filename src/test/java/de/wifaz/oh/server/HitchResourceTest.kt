package de.wifaz.oh.server

import com.nhaarman.mockitokotlin2.KArgumentCaptor
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.check
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import de.wifaz.oh.protocol.*
import junit.framework.Assert.*
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.mockito.Mockito.reset
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.verifyZeroInteractions
import java.net.HttpURLConnection
import java.security.Principal
import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.Response
import javax.ws.rs.core.SecurityContext


internal class HitchResourceTest : TestData() {

    @Test
    fun createUser() {
        val db: HitchDB = getDB()
        val messengerMock: Messenger = mock()
        val matcherMock: Matcher = mock()

        val hitchResource = HitchResource(db, messengerMock, matcherMock)
        val testUser = User("idTestUser", "Abdul", "Nachtigaller")
        val createUserResponse = hitchResource.createUser(testUser)

        assertTrue(createUserResponse.token != null)
        createUserResponse.registration_state

        assertEquals(testUser, db.getUser(testUser.id))
        assertEquals(testUser.id, db.getTokenUser(TokenHelper.asDbHash(createUserResponse.token)))
        val expectedRegistrationState = RegistrationState(
            user_id = testUser.id,
            has_profile_picture = false,
            phone_verified = false,
            fully_registered = false
        )
        assertEquals(expectedRegistrationState, createUserResponse.registration_state)
        assertEquals(expectedRegistrationState, db.getRegistrationState(testUser.id))
    }

    @Test
    fun updateUser() {
        val db: HitchDB = getDB()
        val messengerMock: Messenger = mock()
        val matcherMock: Matcher = mock()
        val securityContextMock = getSecurityContextMock(DRIVER)

        val hitchResource = HitchResource(db, messengerMock, matcherMock)
        createTestUsers(hitchResource, db)

        val response: Response =
            hitchResource.updateUser(DRIVER.id, DRIVER.copy(first_name = "Emil"), securityContextMock)
        assertEquals(Response.Status.OK.statusCode, response.status)
        assertEquals(DRIVER.copy(first_name = "Emil"), db.getUser(DRIVER.id))
    }

    @Test
    fun deleteUser() {
        val db: HitchDB = getDB()
        val messengerMock: Messenger = mock()
        val matcherMock: Matcher = mock()
        val securityContextMock = getSecurityContextMock(DRIVER)

        val hitchResource = HitchResource(db, messengerMock, matcherMock)
        createTestUsers(hitchResource, db)

        val response: Response = hitchResource.deleteUser(DRIVER.id, securityContextMock)
        assertEquals(Response.Status.OK.statusCode, response.status)
        assertFalse(db.hasUser(DRIVER.id))
    }


    @Test
    fun createWay() {
        val db: HitchDB = getDB()
        val messengerMock: Messenger = mock()
        val matcherMock: Matcher = mock()
        val securityContextMock = getSecurityContextMock(DRIVER)

        val hitchResource = HitchResource(db, messengerMock, matcherMock)
        createTestUsers(hitchResource, db)

        // create some way
        var response: Response = hitchResource.createWay(WAY_DRIVER, securityContextMock)
        assertEquals(
            HttpURLConnection.HTTP_OK,
            response.status
        )  // FIXME should become HTTP_CREATED when using Method CREATE
        verifyZeroInteractions(messengerMock)

        // create an other, non conflicting, non interacting way
        response = hitchResource.createWay(WAY2_DRIVER, securityContextMock)
        assertEquals(
            HttpURLConnection.HTTP_OK,
            response.status
        )  // FIXME should become HTTP_CREATED when using Method CREATE
        verifyZeroInteractions(messengerMock)
    }

    @Test
    fun deleteWay() {
        val db: HitchDB = getDB()
        val messengerMock: Messenger = mock()
        val matcherMock: Matcher = mock()


        // Mock two different SecurityContexts to mimic access of two different users
        val securityContextMockDriver = getSecurityContextMock(DRIVER)
        val securityContextMockPassenger = getSecurityContextMock(PASSENGER)

        val hitchResource = HitchResource(db, messengerMock, matcherMock)
        createTestUsers(hitchResource, db)

        // create some way as DRIVER
        var response: Response = hitchResource.createWay(WAY_DRIVER, securityContextMockDriver)
        assertEquals(
            HttpURLConnection.HTTP_OK,
            response.status
        )  // FIXME should become HTTP_CREATED when using Method CREATE
        verifyZeroInteractions(messengerMock)

        // delete it
        response = hitchResource.deleteWay(WAY_DRIVER.id, securityContextMockDriver)
        assertEquals(HttpURLConnection.HTTP_OK, response.status)  // FIXME should be HTTP_NO_CONTENT
        assertFalse(db.hasWay(WAY_DRIVER.id))

        verifyZeroInteractions(messengerMock)
    }

    private fun getSecurityContextMock(user: User): SecurityContext {
        val securityContextMock: SecurityContext = mock()
        val principalMock: Principal = mock()
        whenever(securityContextMock.userPrincipal).thenReturn(principalMock)
        whenever(principalMock.name).thenReturn(user.id)
        return securityContextMock
    }

    @Test
    fun match() {
        val db: HitchDB = getDB()
        val messengerMock: Messenger = mock()
        val matcherMock: Matcher = mock()
        val securityContextMockPassenger = getSecurityContextMock(PASSENGER)
        val securityContextMockDriver = getSecurityContextMock(DRIVER)


        whenever(matcherMock.match(WAY_PASSENGER, WAY_DRIVER)).thenReturn(LIFT1)

        val hitchResource = HitchResource(db, messengerMock, matcherMock)
        createTestUsers(hitchResource, db)

        hitchResource.createWay(WAY_DRIVER, securityContextMockDriver)
        hitchResource.createWay(WAY_PASSENGER, securityContextMockPassenger)

        checkReceivedLIFT1(messengerMock)
        verifyNoMoreInteractions(messengerMock)
    }

    @Test
    fun matchWhenPassengerPublishesBeforeDriver() {
        val db: HitchDB = getDB()
        val messengerMock: Messenger = mock()
        val matcherMock: Matcher = mock()
        val securityContextMockPassenger = getSecurityContextMock(PASSENGER)
        val securityContextMockDriver = getSecurityContextMock(DRIVER)

        whenever(matcherMock.match(WAY_PASSENGER, WAY_DRIVER)).thenReturn(LIFT1)

        val hitchResource = HitchResource(db, messengerMock, matcherMock)
        createTestUsers(hitchResource, db)

        hitchResource.createWay(WAY_PASSENGER, securityContextMockPassenger)
        checkReceivedEmptyLiftList(messengerMock)
        verifyNoMoreInteractions(messengerMock)
        reset(messengerMock)

        hitchResource.createWay(WAY_DRIVER, securityContextMockDriver)
        checkReceivedLIFT1(messengerMock)
        verifyNoMoreInteractions(messengerMock)
    }

    private fun checkReceivedLIFT1(messengerMock: Messenger) {
        val expected = setOf(
            LiftInfoMessageData(
                PASSENGER.id,
                WAY_PASSENGER.id,
                listOf(LiftInfo(LIFT1, Role.PASSENGER, DRIVER, WAY_DRIVER))
            )
        )
        val received = getReceivedLiftInfoMessages(messengerMock, 1)
        assertEquals(expected, received)
    }

    private fun checkReceivedEmptyLiftList(messengerMock: Messenger) {
        verify(messengerMock).send(
            check {
                assertEquals(WAY_PASSENGER.user_id, it)
            },
            check {
                assertEquals(WAY_PASSENGER.id, it.reference_way_id)
                assertEquals(LiftInfoMessage::class.java, it.javaClass)
                val lim = it as LiftInfoMessage
                assertEquals(0, lim.lift_infos.size)
            }
        )
    }


    @Test
    fun normalStatusPromotion() {
        val db: HitchDB = getDB()
        val messengerMock: Messenger = mock()
        val matcherMock: Matcher = mock()
        val securityContextMockPassenger = getSecurityContextMock(PASSENGER)
        val securityContextMockDriver = getSecurityContextMock(DRIVER)


        // Mock match to get a lift into the database
        whenever(matcherMock.match(WAY_PASSENGER, WAY_DRIVER)).thenReturn(LIFT1)
        val hitchResource = HitchResource(db, messengerMock, matcherMock)
        createTestUsers(hitchResource, db)

        hitchResource.createWay(WAY_DRIVER, securityContextMockDriver)
        hitchResource.createWay(WAY_PASSENGER, securityContextMockPassenger)
        checkReceivedLIFT1(messengerMock)
        verifyNoMoreInteractions(messengerMock)
        reset(messengerMock)

        hitchResource.updateLiftStatus(LIFT1.id, Lift.Status.REQUESTED, securityContextMockPassenger)

        verify(messengerMock).send(
            check {
                assertEquals(DRIVER.id, it)
            },
            check {
                assertEquals(WAY_DRIVER.id, it.reference_way_id)
                assertEquals(LiftInfoMessage::class.java, it.javaClass)
                val lim = it as LiftInfoMessage
                assertEquals(lim.reference_way_id, WAY_DRIVER.id)
                assertEquals(1, lim.lift_infos.size)
                val liftInfo = lim.lift_infos[0]
                assertEquals(LIFT1.copy(status = Lift.Status.REQUESTED), liftInfo.lift)
            }
        )
        verifyNoMoreInteractions(messengerMock)

        reset(messengerMock)

        hitchResource.updateLiftStatus(LIFT1.id, Lift.Status.ACCEPTED, securityContextMockDriver)
        verify(messengerMock).send(
            check { assertEquals(PASSENGER.id, it) },
            check {
                assertEquals(WAY_PASSENGER.id, it.reference_way_id)
                assertEquals(LiftStatusMessage::class.java, it.javaClass)
                val lsm = it as LiftStatusMessage
                assertEquals(Lift.Status.ACCEPTED, lsm.status)
                assertEquals(LIFT1.id, lsm.lift_id)
            }
        )
        verifyNoMoreInteractions(messengerMock)
    }

    @Test
    fun securityTest() {
        val db: HitchDB = getDB()
        val messengerMock: Messenger = mock()
        val matcherMock: Matcher = mock()
        val securityContextMockPassenger = getSecurityContextMock(PASSENGER)
        val securityContextMockDriver = getSecurityContextMock(DRIVER)
        val hitchResource = HitchResource(db, messengerMock, matcherMock)
        createTestUsers(hitchResource, db)

        // update other user
        assertThrows(WebApplicationException::class.java) {
            hitchResource.updateUser(DRIVER.id, DRIVER.copy(first_name = "Heino"), securityContextMockPassenger)
        }
        assertEquals(DRIVER.first_name, db.getUser(DRIVER.id).first_name)

        // delete other user
        assertThrows(WebApplicationException::class.java) {
            hitchResource.deleteUser(DRIVER.id, securityContextMockPassenger)
        }
        assertTrue(db.hasUser(DRIVER.id))

        // create way for other user
        assertThrows(WebApplicationException::class.java) {
            hitchResource.createWay(WAY_DRIVER, securityContextMockPassenger)
        }
        assertFalse(db.hasWay(WAY_DRIVER.id))

        // update way status for other user
        hitchResource.createWay(WAY_DRIVER, securityContextMockDriver)
        assertThrows(WebApplicationException::class.java) {
            hitchResource.updateWayStatus(WAY_DRIVER.id, Way.Status.STARTED, securityContextMockPassenger)
        }
        assertEquals(WAY_DRIVER.status, db.getWay(WAY_DRIVER.id).status)

        // delete way for other user
        assertThrows(WebApplicationException::class.java) {
            hitchResource.deleteWay(WAY_DRIVER.id, securityContextMockPassenger)
        }
        assertTrue(db.hasWay(WAY_DRIVER.id))
    }

    @Test
    fun autoRequest() {
        val db: HitchDB = getDB()
        val messengerMock: Messenger = mock()
        val matcherMock: Matcher = mock()
        val securityContextMockPassenger = getSecurityContextMock(PASSENGER)
        val securityContextMockDriver = getSecurityContextMock(DRIVER)

        val wayPassenger = WAY_PASSENGER.copy(autocommit = true)
        whenever(matcherMock.match(wayPassenger, WAY_DRIVER)).thenReturn(LIFT1)

        val hitchResource = HitchResource(db, messengerMock, matcherMock)
        createTestUsers(hitchResource, db)

        hitchResource.createWay(WAY_DRIVER, securityContextMockDriver)
        hitchResource.createWay(wayPassenger, securityContextMockPassenger)

        val autoRequestedLift = LIFT1.copy(status = Lift.Status.REQUESTED)
        val expected = setOf(
            LiftInfoMessageData(
                PASSENGER.id,
                wayPassenger.id,
                listOf(LiftInfo(autoRequestedLift, Role.PASSENGER, DRIVER, WAY_DRIVER))
            ),
            LiftInfoMessageData(
                DRIVER.id,
                WAY_DRIVER.id,
                listOf(LiftInfo(autoRequestedLift, Role.DRIVER, PASSENGER, wayPassenger))
            ),
        )
        val received = getReceivedLiftInfoMessages(messengerMock, 2)
        assertEquals(expected, received)
    }

    @Test
    fun noAutoRequestOnSectionalMatch() {
        val db: HitchDB = getDB()
        val messengerMock: Messenger = mock()
        val matcherMock: Matcher = mock()
        val securityContextMockPassenger = getSecurityContextMock(PASSENGER)
        val securityContextMockDriver = getSecurityContextMock(DRIVER)

        val wayPassenger = WAY_PASSENGER.copy(autocommit = true)
        val lift = LIFT1.copy(sectional = true)
        whenever(matcherMock.match(wayPassenger, WAY_DRIVER)).thenReturn(lift)

        val hitchResource = HitchResource(db, messengerMock, matcherMock)
        createTestUsers(hitchResource, db)

        hitchResource.createWay(WAY_DRIVER, securityContextMockDriver)
        hitchResource.createWay(wayPassenger, securityContextMockPassenger)

        val expected = setOf(
            LiftInfoMessageData(
                PASSENGER.id,
                wayPassenger.id,
                listOf(LiftInfo(lift, Role.PASSENGER, DRIVER, WAY_DRIVER))
            )
        )
        val received = getReceivedLiftInfoMessages(messengerMock, 1)
        assertEquals(expected, received)
    }

    @Test
    fun autoRequestNewOffer() {
        val db: HitchDB = getDB()
        val messengerMock: Messenger = mock()
        val matcherMock: Matcher = mock()
        val securityContextMockPassenger = getSecurityContextMock(PASSENGER)
        val securityContextMockDriver = getSecurityContextMock(DRIVER)

        val wayPassenger = WAY_PASSENGER.copy(autocommit = true)
        whenever(matcherMock.match(wayPassenger, WAY_DRIVER)).thenReturn(LIFT1)

        val hitchResource = HitchResource(db, messengerMock, matcherMock)
        createTestUsers(hitchResource, db)

        hitchResource.createWay(wayPassenger, securityContextMockPassenger)
        hitchResource.createWay(WAY_DRIVER, securityContextMockDriver)

        val autoRequestedLift = LIFT1.copy(status = Lift.Status.REQUESTED)
        val expected = setOf(
            LiftInfoMessageData(PASSENGER.id, wayPassenger.id, emptyList()),
            LiftInfoMessageData(
                PASSENGER.id,
                wayPassenger.id,
                listOf(LiftInfo(autoRequestedLift, Role.PASSENGER, DRIVER, WAY_DRIVER))
            ),
            LiftInfoMessageData(
                DRIVER.id,
                WAY_DRIVER.id,
                listOf(LiftInfo(autoRequestedLift, Role.DRIVER, PASSENGER, wayPassenger))
            ),
        )
        val received = getReceivedLiftInfoMessages(messengerMock, 3)
        assertEquals(expected, received)
    }

    @Test
    fun multipleAutoRequest() {
        val db: HitchDB = getDB()
        val messengerMock: Messenger = mock()
        val matcherMock: Matcher = mock()
        val securityContextMockPassenger = getSecurityContextMock(PASSENGER)
        val securityContextMockDriver = getSecurityContextMock(DRIVER)
        val driver2 = DRIVER.copy(id = "idDriver2")
        val securityContextMockDriver2 = getSecurityContextMock(driver2)
        val wayDriver2 = WAY_DRIVER.copy(id = "idWayDriver2", user_id = "idDriver2")

        val wayPassenger = WAY_PASSENGER.copy(autocommit = true)
        whenever(matcherMock.match(wayPassenger, WAY_DRIVER)).thenReturn(LIFT1)
        val lift2 = LIFT1.copy(id = "idLift2", driver_way_id = wayDriver2.id, rating = LIFT1.rating + 1)
        whenever(matcherMock.match(wayPassenger, wayDriver2)).thenReturn(lift2)

        val hitchResource = HitchResource(db, messengerMock, matcherMock)
        createTestUsers(hitchResource, db)
        createTestUser(hitchResource, db, driver2)

        hitchResource.createWay(wayPassenger, securityContextMockPassenger)
        hitchResource.createWay(WAY_DRIVER, securityContextMockDriver)
        hitchResource.createWay(wayDriver2, securityContextMockDriver2)

        val autoRequestedLift = LIFT1.copy(status = Lift.Status.REQUESTED)
        val autoRequestedLift2 = lift2.copy(status = Lift.Status.REQUESTED)
        val expected = setOf(
            LiftInfoMessageData(PASSENGER.id, wayPassenger.id, emptyList()),
            LiftInfoMessageData(
                PASSENGER.id,
                wayPassenger.id,
                listOf(LiftInfo(autoRequestedLift, Role.PASSENGER, DRIVER, WAY_DRIVER))
            ),
            LiftInfoMessageData(
                PASSENGER.id,
                wayPassenger.id,
                listOf(LiftInfo(autoRequestedLift2, Role.PASSENGER, driver2, wayDriver2))
            ),
            LiftInfoMessageData(
                DRIVER.id,
                WAY_DRIVER.id,
                listOf(LiftInfo(autoRequestedLift, Role.DRIVER, PASSENGER, wayPassenger))
            ),
            LiftInfoMessageData(
                driver2.id,
                wayDriver2.id,
                listOf(LiftInfo(autoRequestedLift2, Role.DRIVER, PASSENGER, wayPassenger))
            ),
        )
        val received = getReceivedLiftInfoMessages(messengerMock, 5)
        assertEquals(expected, received)

        // Check, that when one Lift is accepted, the other ones will switch from REQUESTED to PASSENGER_CANCELED
        reset(messengerMock)
        hitchResource.updateLiftStatus(lift2.id, Lift.Status.ACCEPTED, securityContextMockDriver2)

        val expectedLSM = setOf(
            LiftStatusMessageData(PASSENGER.id, WAY_PASSENGER.id, lift2.id, Lift.Status.ACCEPTED),
            LiftStatusMessageData(DRIVER.id, WAY_DRIVER.id, LIFT1.id, Lift.Status.WITHDRAWN),
            LiftStatusMessageData(PASSENGER.id, WAY_PASSENGER.id, LIFT1.id, Lift.Status.WITHDRAWN),
        )
        val receivedLSM = getReceivedLiftStatusMessages(messengerMock, 3)
        assertEquals(expectedLSM, receivedLSM)

    }


    data class LiftInfoMessageData(
        val topic: String,
        val referenceWayId: String,
        val liftInfos: List<LiftInfo>
    )

    fun getReceivedLiftInfoMessages(messengerMock: Messenger, n: Int): Set<LiftInfoMessageData> {
        val userIdCaptor: KArgumentCaptor<String> = argumentCaptor()
        val liftInfoMessageCaptor: KArgumentCaptor<LiftInfoMessage> = argumentCaptor()
        verify(messengerMock, times(n)).send(userIdCaptor.capture(), liftInfoMessageCaptor.capture())
        return (userIdCaptor.allValues zip liftInfoMessageCaptor.allValues).map {
            LiftInfoMessageData(it.first, it.second.reference_way_id, it.second.lift_infos)
        }.toSet()
    }

    data class LiftStatusMessageData(
        val topic: String,
        val referenceWayId: String,
        val liftId: String,
        val status: Lift.Status
    )

    fun getReceivedLiftStatusMessages(messengerMock: Messenger, n: Int): Set<LiftStatusMessageData> {
        val userIdCaptor: KArgumentCaptor<String> = argumentCaptor()
        val liftStatusMessageCaptor: KArgumentCaptor<LiftStatusMessage> = argumentCaptor()
        verify(messengerMock, times(n)).send(userIdCaptor.capture(), liftStatusMessageCaptor.capture())
        return (userIdCaptor.allValues zip liftStatusMessageCaptor.allValues).map {
            LiftStatusMessageData(it.first, it.second.reference_way_id, it.second.lift_id, it.second.status)
        }.toSet()
    }

    @Test
    fun onRemoveLift() {
    }

    private fun getDB(): RamHitchDB {
        var db = RamHitchDB(HitchDBHook(mock()))
        return db
    }

    private fun createTestUsers(hitchResource: HitchResource, db: HitchDB) {
        createTestUser(hitchResource, db, DRIVER)
        createTestUser(hitchResource, db, PASSENGER)
    }

    private fun createTestUser(hitchResource: HitchResource, db: HitchDB, user: User) {
        hitchResource.createUser(user)
        val fullyRegistered = RegistrationState(
            user_id = user.id,
            has_profile_picture = true,
            phone_verified = false,
            fully_registered = true
        )
        db.putRegistrationState(fullyRegistered)
    }

}