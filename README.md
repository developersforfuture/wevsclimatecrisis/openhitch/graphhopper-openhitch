This repository is the server part of project [OpenHitch](https://gitlab.com/developersforfuture/wevsclimatecrisis/openhitch/openhitch/-/blob/main/README.md).
It is based on the [GraphHopper](https://github.com/graphhopper/graphhopper) route planning server.
